DOCUMENTS = ../Efficient-Parallel-Abstract-Interpreter-in-Scala--extended-bibliography--Olivier-Pirson-2019.pdf

.SUFFIXES:

BIBTEX      = biber
BIBTEXFLAGS =

DVIPS      = dvips
DVIPSFLAGS =

MAKEINDEX      = makeindex
MAKEINDEXFLAGS =

PSPDF      = ps2pdf
PSPDFFLAGS = -sPAPERSIZE=a4

TEX      = latex
TEXFLAGS =


RM    = rm -f
SHELL = sh



###
# #
###
.PHONY:	all

all:	$(DOCUMENTS)



#########
# Rules #
#########
.PRECIOUS: %.aux %.bcf %.dvi %.idx %.ind %.ps

%.dvi:	%.tex
	$(TEX) $(TEXFLAGS) $<
ifeq ($(QUICK),)
	$(BIBTEX) $(BIBTEXFLAGS) $(basename $<)
	$(TEX) $(TEXFLAGS) $<
endif

%.idx:	%.tex
	$(TEX) $(TEXFLAGS) $<

%.ind:	%.idx
	$(MAKEINDEX) $(MAKEINDEXFLAGS) $<

../%.pdf:	%.ps
	$(PSPDF) $(PSPDFFLAGS) $< $@

%.ps:	%.dvi
	$(DVIPS) $(DVIPSFLAGS) -o $@ $<



################
# Dependancies #
################
Efficient-Parallel-Abstract-Interpreter-in-Scala--extended-bibliography--Olivier-Pirson-2019.dvi:	sty/opireport.sty ../bib/mathesis.bib



#########
# Clean #
#########
.PHONY:	clean cleanDvi cleanPdf cleanPs distclean overclean

clean:
	$(RM) *.aux tex/*.aux
	$(RM) *.bbl *.bcf *.blg *.brf *.idx *.ilg *.ind *.loe *.lof *.log .log *.out *.toc *.tdo
	$(RM) Efficient-Parallel-Abstract-Interpreter-in-Scala--extended-bibliography--Olivier-Pirson-2019.run.xml

cleanDvi:
	$(RM) *.dvi

cleanPdf:
	$(RM) $(DOCUMENTS)

cleanPs:
	$(RM) *.ps

distclean:	clean cleanDvi cleanPs

overclean:	distclean cleanPdf
