.. -*- restructuredtext -*-

=========================================================
*An efficient and parallel abstract interpreter in Scala*
=========================================================
MEMO-F524 *Masters thesis* (ULB) (15.5/20)

* **Final document**:
  `Parallélisation d'un interpréteur abstrait, au travers du modèle acteur — Application à l'interpréteur abstrait Scala-AM`_
  (a `ZIP archive`_ with additional PDF figures)
* **Slides** of `Présentation finale`_ (September 2019) (on Speaker Deck: https://speakerdeck.com/opimedia/parallelisation-dun-interpreteur-abstrait-au-travers-du-modele-acteur )
* **Implementation**: `Scala-Par-AM`_
* `HTML page of benchmark results`_ on Scheme examples

.. _`HTML page of benchmark results`: http://www.opimedia.be/CV/2017-2018-ULB/MEMO-F524-Masters-thesis/benchmark-results/
.. _`Parallélisation d'un interpréteur abstrait, au travers du modèle acteur — Application à l'interpréteur abstrait Scala-AM`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Parallelisation-d-un-interpreteur-abstrait-au-travers-du-modele-acteur--Olivier-Pirson-2019.pdf
.. _`Présentation finale`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Parallelisation-d-un-interpreteur-abstrait-au-travers-du-modele-acteur--Presentation-finale--Olivier-Pirson-2019--slides.pdf
.. _`Scala-Par-AM`: https://bitbucket.org/OPiMedia/scala-par-am/
.. _`ZIP archive`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Parallelisation-d-un-interpreteur-abstrait-au-travers-du-modele-acteur--Olivier-Pirson-2019.zip

    *The first moral of this story is that program testing can be used very efficiently to show the presence of bugs, but never to show their absence.*

    --- `Edsger W. Dijkstra`_,
    `Concern for Correctness as a Guiding Principle for Program Composition (EWD288)`_

.. _`Edsger W. Dijkstra`: https://www.cs.utexas.edu/users/EWD/
.. _`Concern for Correctness as a Guiding Principle for Program Composition (EWD288)`: https://www.cs.utexas.edu/users/EWD/transcriptions/EWD02xx/EWD288.html


* Slides of first `Presentation`_ (November 2017) (on Speaker Deck: https://speakerdeck.com/opimedia/an-efficient-and-parallel-abstract-interpreter-in-scala-presentation )
* Slides of `Second Presentation`_ (March 2018) (on Speaker Deck: https://speakerdeck.com/opimedia/an-efficient-and-parallel-abstract-interpreter-in-scala-second-presentation )
* Slides of `First Algorithm`_ (November 2018) (on Speaker Deck: https://speakerdeck.com/opimedia/an-efficient-and-parallel-abstract-interpreter-in-scala-first-algorithm )
* Slides of `3x3 Parallel Implementations`_ (March 2019) (on Speaker Deck: https://speakerdeck.com/opimedia/efficient-parallel-abstract-interpreter-in-scala-3x3-implementations )
* `Extended bibliography`_
* `Statement of the subject`_

.. _`3x3 Parallel Implementations`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala---3x3-implementations--Olivier-Pirson-20190322--slides.pdf
.. _`Extended bibliography`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala--extended-bibliography--Olivier-Pirson-2019.pdf
.. _`First Algorithm`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala--First-Algorithm--Olivier-Pirson-20181112--slides.pdf
.. _`Presentation`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala--Presentation--Olivier-Pirson-20171127--slides.pdf
.. _`Second Presentation`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/raw/master/Efficient-Parallel-Abstract-Interpreter-in-Scala--Second-Presentation--Olivier-Pirson-20180320--slides.pdf
.. _`Statement of the subject`: https://web.archive.org/web/20181229145848/http://soft.vub.ac.be/soft/proposal/efficient-and-parallel-abstract-interpreter-scala



All documents and LaTeX sources are available on this Bitbucket repository:
https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala

|



Author: 🌳 Olivier Pirson — OPi |OPi| 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
=================================================================
🌐 Website: http://www.opimedia.be/

💾 Bitbucket: https://bitbucket.org/OPiMedia/

* 📧 olivier.pirson.opi@gmail.com
* Mastodon: https://mamot.fr/@OPiMedia — Twitter: https://twitter.com/OPirson
* 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
* other profiles: http://www.opimedia.be/about/

.. |OPi| image:: http://www.opimedia.be/_png/OPi.png



|An efficient and parallel abstract interpreter in Scala|

(piece of René Magritte, `Le Calcul Mental`_, 1940)

.. _`Le Calcul Mental`: http://www.artnet.com/artists/ren%C3%A9-magritte/le-calcul-mental-oU6yWQzE-ERgxZSTgANE-g2

.. |An efficient and parallel abstract interpreter in Scala| image:: https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Jun/18/4285472120-5-efficient-parallel-abstract-interpret_avatar.png
