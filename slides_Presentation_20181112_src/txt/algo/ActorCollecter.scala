final class ActorCollecter extends Actor {
  import ActorCollecter._

  var mainSender: ActorRef = null
  var newTodo: List[State] = null
  var newHalted: Set[State] = null
  var newGraph: Graph = null
  var nb: Int = -1  // count AllStarted received + the number of state results receive
  var nbStateWaited: Int = -1
  var newVisited: VS[State] = VisitedSet[VS].empty

  def increment() = {
    nb += 1
    if (nb > nbStateWaited)  // all started and finished all states, then send results
      mainSender ! (newTodo, newVisited, newHalted, newGraph)  // to caller loop
  }

  override def receive = {
    case NewState(state: State, successors: Set[State]) =>
      newGraph = newGraph.map(_.addEdges(state, successors))
      newTodo ++= successors
      increment
    case Already => increment
    case Subsumed => increment
    case Halted(state: State) =>
      newHalted += state
      increment
$\dots$
