final class ActorStarter extends Actor {
  import ActorStarter._

  var todo: List[State] = null
  var newVisited: VS[State] = VisitedSet[VS].empty
  var actorI: Int = -1
  var nb: Int = -1

  override def receive = {
    case Init(todo: List[State], visited, halted: Set[State], graph: Graph) =>
      this.todo = todo
      newVisited = visited
      actorI = 0
      nb = 0
      actorCollecter ! ActorCollecter.Init(halted, graph, sender)

    case Start =>
      for (state <- todo) {
        if (actors(actorI) == null)  // create new actor
          actors(actorI) = actorSystem.actorOf(Props(new ActorState))

        actors(actorI) ! ActorState.Eval(state, newVisited, actorCollecter)
        newVisited = VisitedSet[VS].add(newVisited, state)

        actorI += 1
        if (actorI == maxActorStates) actorI = 0
        nb += 1
      }

      actorCollecter ! ActorCollecter.AllStarted(newVisited, nb)
  }
}
$\dots$
