$\dots$
    case Init(halted: Set[State], graph: Graph, mainSender: ActorRef) =>
      this.mainSender = mainSender
      newTodo = Nil
      newHalted = halted
      newGraph = graph
      nb = 0
      nbStateWaited = Int.MaxValue  // biggest value to avoid end before init par AllStarted
      newVisited = VisitedSet[VS].empty
      sender ! ActorStarter.Start  // to actorStarter
    case AllStarted(visited, nbStateWaited: Int) =>
      this.nbStateWaited = nbStateWaited
      newVisited = visited
      increment
  }
}
object ActorCollecter {
  val Already = 2
  val Subsumed = 3
  case class NewState(state: State, successors: Set[State])
  case class Halted(state: State)
  case class Init(halted: Set[State], graph: Graph, mainSender: ActorRef)
  case class AllStarted(visited: VS[State], nbStateWaited: Int)
}

val actorCollecter: ActorRef = actorSystem.actorOf(Props(new ActorCollecter))
