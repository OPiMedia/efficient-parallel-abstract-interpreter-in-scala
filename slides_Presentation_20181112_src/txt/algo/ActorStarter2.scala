$\dots$
object ActorStarter {
  val Start = 1
  case class Init(todo: List[State], visited: VS[State], halted: Set[State], graph: Graph)
}

val actorStarter: ActorRef = actorSystem.actorOf(Props(new ActorStarter))
