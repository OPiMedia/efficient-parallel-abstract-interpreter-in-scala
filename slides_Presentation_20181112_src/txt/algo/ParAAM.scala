@scala.annotation.tailrec
def loop(todo: List[State], visited: VS[State], halted: Set[State], graph: Graph):
    ParAAMOutput = todo match {
  case Nil => {           // finished, the worklist todo is empty
    actorSystem.terminate
    ParAAMOutput(halted, VisitedSet[VS].size(visited), timeout.time, graph, false, stats)
  }

  case _ =>
    if (timeout.reached)  // exceeded the maximal time allowed, stop
      ParAAMOutput(halted, VisitedSet[VS].size(visited), timeout.time, graph, true, stats)
    else {                // will send each state from worklist todo to ActorState
      val future = actorStarter ? ActorStarter.Init(todo, visited, halted, graph)

      val (newTodo, newVisited, newHalted, newGraph) =
        Await.result(future, Timeout("some" seconds).duration)
          .asInstanceOf[(List[State], VS[State], Set[State], Graph)]

      loop(newTodo, newVisited, newHalted, newGraph)
    }
}
