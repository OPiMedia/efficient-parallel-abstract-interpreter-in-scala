final class ActorState extends Actor {
  import ActorState.Eval

  override def receive = {
    case Eval(state: State, visited, actorCollecter: ActorRef) =>
      if (VisitedSet[VS].contains(visited, state))  // already
        actorCollecter ! ActorCollecter.Already
      else if (subsumption &&                       // subsumed
        VisitedSet[VS].exists(visited, state, (s2: State) => s2.subsumes(state)))
        actorCollecter ! ActorCollecter.Subsumed
      else if (state.halted)                        // halted stated
        actorCollecter ! ActorCollecter.Halted(state)
      else                                          // new state
        actorCollecter ! ActorCollecter.NewState(state, state.step(sem))
  }
}
object ActorState {
  case class Eval(state: State, visited: VS[State], actorCollecter: ActorRef)
}
