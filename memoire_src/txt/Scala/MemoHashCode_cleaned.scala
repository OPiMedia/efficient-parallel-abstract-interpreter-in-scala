trait MemoHashCode extends Product {
  private lazy val memoizedHashCode: Int =
    scala.runtime.ScalaRunTime._hashCode(this)
  override def hashCode: Int = memoizedHashCode
}
