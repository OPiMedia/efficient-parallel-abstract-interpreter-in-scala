/**
  * Simple exemple d'utilisation d'acteurs avec Akka.
  */
package be.opimedia.newId

/**
  * Instancie et d�marre un acteur NewId et deux acteurs User
  * qui demande chacun un nouvel id et l'imprime une fois re�u.
  */
object NewIdApp extends App {
  import akka.actor.{Actor, ActorRef, ActorSystem, Props}
  import akka.pattern.ask  // importe l'op�rateur ?
  import akka.util.Timeout
  import scala.concurrent.{Await, Future}
  import scala.concurrent.duration._
  import scala.language.postfixOps

  // Timeout par d�faut pour chaque nouvel acteur
  implicit val _ = Timeout(3 seconds)


  // Acteur pour envoyer des nouveaux ids � des acteurs User
  object NewId {
    // Les messages sont d�finis dans l'objet compagnon
    val GetNextId: Int = 0  // message constant
    val Stop: Int = 1
  }

  class NewId extends Actor {
    var lastIdUsed: Int = -1

    // Importe les messages de l'objet compagnon
    import NewId.{GetNextId, Stop}

    override
    def receive = {  // traite les messages re�us
      case GetNextId =>
        lastIdUsed += 1
        // Envoie ce nouvel id � l'exp�diteur du message
        sender ! User.Id(lastIdUsed)

      case Stop =>
        context.stop(self)  // stoppe cet acteur

      // N'importe quel autre message (utile pour d�boguer)
      case x =>
        println(s"NewId unknow message! $x")
        System.out.flush  // vide le tampon, affichage direct
    }
  }


  // Lance le syst�me g�rant les acteurs
  val actorSystem: ActorSystem =
    ActorSystem("Akka-System-NewIdApp")

  // Instancie un acteur NewId
  val newId: ActorRef =
    actorSystem.actorOf(Props(new NewId), "newId")


  // Acteur interagissant avec NewId
  object User {
    val Start: Int = 0  // message constant
    case class Id(id: Int)  // message avec param�tre
  }

  class User extends Actor {
    import User.{Start, Id}

    var mainActor: ActorRef = null

    override
    def receive = {
      case Start =>
        // Sauve la r�f�rence de l'acteur principal
        mainActor = sender
        // Envoie un message GetNextId � l'acteur newId
        newId ! NewId.GetNextId

      case Id(id: Int) =>  // re�oit un id avec ce message
        println(s"${self.path.name}: $id")
        System.out.flush

        // Envoie message (quelconque) � l'acteur principal
        mainActor ! Nil

      case x =>
        println(s"${self.path.name} unknow message! $x")
        System.out.flush
    }
  }



  // Main
  val user0: ActorRef =
    actorSystem.actorOf(Props(new User), "user0")
  val user1: ActorRef =
    actorSystem.actorOf(Props(new User), "user1")

  user0 ! User.Start  // envoie un message Start

  // Envoie un message Start et r�cup�re une future
  val future: Future[Any] = user1 ? User.Start

  // Attend n'importe quel message de user1
  Await.ready(future, 10 seconds)

  newId ! NewId.Stop  // envoie un message Stop

  // Stoppe tous les acteurs restants et ferme le syst�me
  actorSystem.terminate
}
