worklist = $\algotext{ensemble vide}$
visited = $\algotext{ensemble vide}$
halted = $\algotext{ensemble vide}$
graph = $\algotext{graphe vide}$

worklist = worklist $\cup\ \{\algotext{�tat initial}\}$
repeat
  state = $\algotext{prendre un �tat de}$ worklist
  if state $\notin$ visited then
    visited = visited $\cup\ \{$state$\}$
    if state $\algotext{est un �tat final}$ then
      halted = halted $\cup\ \{$state$\}$
    else
      successors = step(state)
      worklist = worklist $\cup$ successors
      graph = graph$\ \cup\ \{\algotext{ar�te}$ state $\rightarrow s \mid s \in\,$successors$\}$
until worklist $\algotext{est vide}$
