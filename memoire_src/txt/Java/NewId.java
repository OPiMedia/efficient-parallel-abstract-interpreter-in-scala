public class NewId {
    private int lastIdUsed = -1;
    public int getNextId() {
        return ++lastIdUsed;
    }
}
