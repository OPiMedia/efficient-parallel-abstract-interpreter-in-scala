% -*- coding: utf-8 -*-
\myChapter{Lois d'\textsc{Amdahl} et de \textsc{Gustafson}-\textsc{Barsis}}
\label{anx:parallelisme}%
\IndexAmdahl
\IndexGustafson
\IndexBarsis
Les lois d'\textsc{Amdahl} et de \textsc{Gustafson}-\textsc{Barsis}
formalisent le constat que l'accélération possible de la parallélisation est limitée
par la partie de programme qui est nécessairement séquentielle.
Voir l'illustration
dans la section \NumNameRef{sec:parallelisme} p. \pageref{fig:limite_partie_sequentielle}.
Nous l'avons déjà présentée p. \pageref{loiAmdahl}. Rappelons-la.

\begin{law}[Loi d'\textsc{Amdahl}]
  \index{loi!de Amdahl@de \textsc{Amdahl}}%
  Soit $\alpha \in [0,1]$ la partie d'un programme parallèle
  dont l'exécution est nécessairement séquentielle.
  Donc $(1 - \alpha)$ représente la partie parallélisable.
  L'accélération maximale possible avec $p$ processeurs est donnée par\\
  $\speedup(p)
  = \frac{\Tseq}{\Tpar}
  = \frac{\Tseq}{\alpha \Tseq + (1 - \alpha) \frac{\Tseq}{p}}
  = \frac{p}{1 + (p - 1) \alpha}$.
\end{law}

Selon cette loi
pour une valeur de $\alpha$ fixée non nul l'accélération maximale est bornée par
une asymptote horizontale de hauteur
$\lim\limits_{p\rightarrow\infty} \speedup(p) = \frac{1}{\alpha}$,
quelque soit le nombre de processeurs utilisés.

Nous pouvons aussi en déduire l'efficacité maximale
et le surcout minimal correspondant,
ainsi que leur limite.
Ils sont résumés dans la table de la page suivante.

\bigskip
Cette loi d'\textsc{Amdahl} peut être critiquée.
Elle considère que les programmes sont fixes
et les ordinateurs évoluent.
Cependant le constat est que plus nous avons de la puissance
et plus nous traitons des données de grande taille.
Cela a amené \textsc{Gustafson} et \textsc{Barsis}
à formuler une nouvelle loi partant d'un temps fixe, aux résultats plus optimistes.
L'augmentation du volume de données traitées par la partie parallèle
minimise de fait l'importance d'une partie séquentielle constante.
\cite[60--61]{mccool_structured_2012}

\begin{law}[Loi de \textsc{Gustafson}-\textsc{Barsis}]
  \cite[2]{gustafson_reevaluating_1988}
  \index{loi!de Gustafson-Barsis@de \textsc{Gustafson}-\textsc{Barsis}}%
  Soit $\alpha \in [0,1]$ la partie d'un programme parallèle
  dont l'exécution est nécessairement séquentielle.\\
  L'accélération échelonnée maximale possible avec $p$ processeurs est donnée par\\
  $\speedup(p)
  = \frac{\alpha + (1 - \alpha) p}{\alpha + (1 - \alpha)}
  = \alpha + (1 - \alpha) p
  = p - \alpha (p - 1)$
\end{law}

De même nous pouvons en déduire l'efficacité maximale
et le surcout minimal correspondant,
et leur limite.
Cette table résume le tout.

\begin{table}[H]\centering
  \hspace*{-2em}%
  \begin{tabular}{@{}|@{\ }l@{\ }||@{\ }l@{\ }|@{\ }l@{\ }||@{\ }l@{\ }|@{\ }l@{\ }|@{}}
    \hline
    & \textsc{Amdahl}
    & $\lim\limits_{p\rightarrow\infty}$
    & \textsc{Gustafson}-\textsc{B.}
    & $\lim\limits_{p\rightarrow\infty}$\\
    \hline
    \hline
    Accélération maximale $\speedup(p)$\rule[-2ex]{0pt}{5ex}
    & $\frac{p}{1 + (p - 1) \alpha}$
    & $\frac{1}{\alpha}$
    & $p - \alpha (p - 1)$
    & $\infty$\\
    \hline
    Efficacité maximale\rule[-2ex]{0pt}{5ex}
    & $\frac{1}{1 + (p - 1) \alpha}$
    & 0
    & $1 - \alpha \left(1 - \frac{1}{p}\right)$
    & $1 - \alpha$\\
    \hline
    Surcout minimal\rule[-2ex]{0pt}{5ex}
    & $\alpha (p - 1) \Tseq$
    & $\infty$
    & $\frac{\alpha (p - 1)}{p - \alpha (p - 1)} \Tseq
    = \frac{1}{\frac{p}{\alpha (p - 1)} - 1} \Tseq$
    & $\frac{\alpha}{1 - \alpha} \Tseq$\\
    \hline
  \end{tabular}\hspace*{-2em}%
  \caption{Résumé des valeurs optimales des métriques de performance\\
    selon les lois d'\textsc{Amdahl} et de \textsc{Gustafson}-\textsc{Barsis}}
\end{table}

Les figures suivantes
comparent les valeurs obtenues pour quelques valeurs de $\alpha$ fixées.

\begin{figureBackgroundHref}\flushleft
  \begin{picture}(0,0)(0,0)
    \put(134,0){\hrefIncludegraphics[width=0.87\textwidth]{parallelisme/loi_Gustafson}}
  \end{picture}%
  \hspace*{-76pt}%
  \hrefIncludegraphics[width=0.87\textwidth]{parallelisme/loi_Amdahl}%
  \caption[Accélération maximale suivant les
           lois d'\textsc{Amdahl} et de \textsc{Gustafson}-\textsc{Barsis}]
          {Accélération maximale suivant les\\
           lois d'\textsc{Amdahl} (à gauche) et de \textsc{Gustafson}-\textsc{Barsis} (à droite),\\
           pour des $\alpha$ de
           {\color[rgb]{0,1,0}0\%}, {\color[rgb]{0,0.63,0}0,1\%},
           {\color[rgb]{0,0,0.25}1\%}, {\color[rgb]{0,0,0.63}2\%},
           {\color[rgb]{0.63,0.5,0}5\%}, {\color[rgb]{1,0.63,0}10\%},
           {\color[rgb]{0.25,0,0}25\%}, {\color[rgb]{0.5,0,0}50\%}, {\color[rgb]{0.63,0,0}75\%} et {\color[rgb]{1,0,0}100\%}}%
  \label{fig:AmdahlVsGustafson}%
\end{figureBackgroundHref}

\begin{figureBackgroundHref}\flushleft
  \begin{picture}(0,0)(0,0)
    \put(138,0){\hrefIncludegraphics[width=0.85\textwidth]{parallelisme/loi_Gustafson_efficacite}}
  \end{picture}%
  \hspace*{-72pt}%
  \hrefIncludegraphics[width=0.85\textwidth]{parallelisme/loi_Amdahl_efficacite}%
  \caption[Efficacité maximale suivant les
           lois d'\textsc{Amdahl} et de \textsc{Gustafson}-\textsc{Barsis}]
          {Efficacité maximale suivant les\\
           lois d'\textsc{Amdahl} (à gauche) et de \textsc{Gustafson}-\textsc{Barsis} (à droite),\\
           pour des $\alpha$ de
           {\color[rgb]{0,1,0}0\%}, {\color[rgb]{0,0.63,0}0,1\%},
           {\color[rgb]{0,0,0.25}1\%}, {\color[rgb]{0,0,0.63}2\%},
           {\color[rgb]{0.63,0.5,0}5\%}, {\color[rgb]{1,0.63,0}10\%},
           {\color[rgb]{0.25,0,0}25\%}, {\color[rgb]{0.5,0,0}50\%}, {\color[rgb]{0.63,0,0}75\%} et {\color[rgb]{1,0,0}100\%}}%
  \label{fig:AmdahlVsGustafsonEfficacite}%
\end{figureBackgroundHref}

\begin{figureBackgroundHref}\flushleft
  \begin{picture}(0,0)(0,0)
    \put(138,0){\hrefIncludegraphics[width=0.85\textwidth]{parallelisme/loi_Gustafson_surcout}}
  \end{picture}%
  \hspace*{-72pt}%
  \hrefIncludegraphics[width=0.85\textwidth]{parallelisme/loi_Amdahl_surcout}%
  \caption[Surcout minimal avec $\Tseq = 1$ suivant les
           lois d'\textsc{Amdahl} et de \textsc{Gustafson}-\textsc{Barsis}]
          {Surcout minimal avec $\Tseq = 1$ suivant les\\
           lois d'\textsc{Amdahl} (à gauche) et de \textsc{Gustafson}-\textsc{Barsis} (à droite),\\
           pour des $\alpha$ de
           {\color[rgb]{0,1,0}0\%}, {\color[rgb]{0,0.63,0}0,1\%},
           {\color[rgb]{0,0,0.25}1\%}, {\color[rgb]{0,0,0.63}2\%},
           {\color[rgb]{0.63,0.5,0}5\%}, {\color[rgb]{1,0.63,0}10\%},
           {\color[rgb]{0.25,0,0}25\%}, {\color[rgb]{0.5,0,0}50\%}, {\color[rgb]{0.63,0,0}75\%} et {\color[rgb]{1,0,0}100\%}}%
  \label{fig:AmdahlVsGustafsonSurcout}%
\end{figureBackgroundHref}



% Local Variables:
%   ispell-local-dictionary: "french"
% End:
