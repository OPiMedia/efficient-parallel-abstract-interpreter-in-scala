% -*- coding: utf-8 -*-
\myChapter{Treillis et correspondance de \textsc{Galois}}
\label{anx:treillis}%
{\setlength{\epigraphwidth}{\textwidth}%
  \epigraphSection
      {\null\hfill
        [\dots] la mathématique est l'art de donner le même nom à des choses différentes.}
      {--- Henri \textsc{Poincaré},\\
        \textit{Science et méthode}, 1908
        \cite[31]{poincare_science_1908}}}

Cette annexe présente la structure mathématique de treillis ainsi que ses propriétés.
\footnote{
  Il s'agit d'une traduction retravaillée d'un chapitre
  du travail préparatoire \cite{pirson_efficient_2017} à ce mémoire,
  lui-même essentiellement constitué d'une synthèse des parties correspondantes
  des mémoires \cite{stievenart_static_2014, de_bleser_static_2016}
  et de la thèse \cite{bouissou_analyse_2008}.}
\footnote{
  Pour plus de détails sur ces structures mathématiques
  vous pouvez consulter \cite{davey_introduction_2002},
  et en français \cite{birkhoff_theorie_1949}.}
Cette structure est utilisée dans le cadre de l'interprétation abstraite
pour formaliser les abstractions et garantir l'existence d'un point fixe.
La correspondance de \textsc{Galois} formalise le fait
que la fonction d'abstraction et la fonction de concrétisation
sont compatibles avec leurs ordres respectifs.

Bien que nécessaire pour la compréhension de la littérature à propos de l'interprétation abstraite,
toutes ces définitions ne sont pas nécessaires à la compréhension de ce mémoire.

\medskip
Avant d'introduire la structure elle-même, définissons-en les notions de base.



\mySection{Ensembles partiellement ordonnés}
\enlargethispage{\baselineskip}%
Soit un ensemble $E$.

\begin{definition}[Relation binaire]\forcenewline
  \index{relation binaire}%
  \indexEnglish{binary relation}{relation binaire}%
  Une \emphDef{relation binaire} \english{binary relation} sur $E$
  est un sous-ensemble de $E \times E$.
\end{definition}

\begin{definition}[Ordre partiel]
  \label{poset}%
  \index{ordre!partiel}%
  \indexEnglish{partially order}{ordre partiel}%
  \indexEnglish{poset}{ordre partiel}%
  \indexSymbol{ordre partiel@$\porder$}%
  Un \emphDef{ordre partiel} \english{partially order, or poset} sur $E$
  est une relation binaire \emphDef{$\porder$} sur $E$
  telle que, $\forall x, y, z \in E$:
  \begin{enumerate}
  \item
    $x \porder x$
    \hspace{11.7em}\index{réflexivité}(\emphDef{réflexivité})
  \item
    $(x \porder y) \conj (y \porder x) \Impl (x = y)$
    \qquad\index{antisymétrie}(\emphDef{antisymétrie})
  \item
    $(x \porder y) \conj (y \porder z) \Impl (x \porder z)$
    \hspace{2.3em}\index{transitivité}(\emphDef{transitivité})
  \end{enumerate}

  \medskip
  \index{ensemble!partiellement ordonné}%
  \indexEnglishSub{set}{partially ordered set}{ensemble partiellement ordonné}%
  \indexSymbol{(E,order)@$(E, \porder)$}%
  Un \emphDef{ensemble partiellement ordonné} \english{partially ordered set}
  \emphDef{$(E, \porder)$}
  est un ensemble $E$ muni d'un ordre partiel $\porder$.

  \medskip
  $\forall x, y \in E : x$ et $y$ sont dits
  \index{comparable}\emphDef{comparables}
  si $x \porder y$ ou $y \porder x$.
\end{definition}

Dans une telle structure deux éléments $x$ et $y$ ne sont pas forcément comparables.
$x \porder y$ exprime qu'ils sont comparables et l'idée que $x$ précède ou est égal à $y$.

\medskip
L'ensemble $E$ peut être un ensemble quelconque.
Toutefois l'abstraction abstraite utilise ces concepts avec des ensembles d'ensembles
(en particulier l'ensemble des parties $\powerset(X)$).
C'est pour cette raison que nous utilisons ici le symbole $\porder$
qui ressemble au symbole d'inclusion $\subseteq$.

\medskip
$\forall$ ensemble $X : (\powerset(X), \subseteq)$ est un ensemble partiellement ordonné.

\bigskip
\label{Hasse}%
\index{diagramme de Hasse@diagramme de \textsc{Hasse}}%
\IndexHasse
\enlargethispage{2.5\baselineskip}\thispagestyle{empty}%
Une façon simple de représenter un ensemble partiellement ordonné \textit{fini}
est de le visualiser sous forme de \emphDef{diagramme de \textsc{Hasse}}.
Il s'agit d'un graphe orienté donc chaque sommet représente un élément de l'ensemble,
et chaque arc montant du sommet $x$ vers le sommet $y$ représente $x \porder y$.
Les boucles découlant de la propriété de réflexivité
et tous les $x \porder y$ pouvant être déduits par transitivité n'étant pas représentés.
Les éléments n'étant pas connectés ne sont pas comparables entre eux.

\begin{figureBackground}%
  \input{tikz/treillis/diagramme_de_hasse_exemple.tex}\hfil
  \input{tikz/treillis/diagramme_de_hasse_parties.tex}%
  \caption[Diagrammes de \textsc{Hasse} de deux exemples]
          {À gauche diagramme de \textsc{Hasse} de l'ensemble partiellement ordonné
    $(\{a, b, c, d, e, f\}, \porder)$
    avec\\
    $\porder = \{(a, a), (a, b), (a, d), (a, f),$
    $(b, b), (b, d), (b, f),$
    $(c, c),$
    $(d, d),$
    $(e, e), (e, f),$
    $(f, f)\}$.\\
    À droite diagramme de \textsc{Hasse}
    de l'ensemble totalement ordonné $(\powerset(\{a, b, c\}), \subseteq)$.}%
    \label{fig:HasseDeuxExemples}%
\end{figureBackground}

%\bigskip
\newpage
Soit un ensemble partiellement ordonné $(P, \porder)$.

\begin{definition}[Ensemble totalement ordonné]\forcenewline
  \index{ensemble!totalement ordonné}%
  \indexEnglishSub{set}{totally ordered set}{ensemble totalement ordonné}%
  \indexEnglish{chain}{ensemble totalement ordonné}%
  $(P, \porder)$ est un \emphDef{ensemble totalement ordonné}
  \english{totally ordered set, or chain}
  si $\forall x, y \in P : x$ et $y$ sont comparables.
\end{definition}

\medskip
$\forall$ ensemble $X : (\powerset(X), \subseteq)$ est un ensemble totalement ordonné.

\bigskip
Pour aider à la manipulation
des structures ordonnées non totales
sont définis les concepts suivants.

\begin{definition}[Borne supérieure]\forcenewline
  \label{borneSupieure}%
  \index{borne!supérieure de X@supérieure de $X$}%
  \indexEnglishSub{bound}{upper bound}{borne supérieure}%
  $\forall X \subseteq P, p \in P$ est une
  \emphDef{borne supérieure} \english{upper bound} \emphDef{de $X$}
  si $\forall x \in X, x \porder p$.

  \medskip
  \index{borne!plus petite borne supérieure de X@plus petite borne supérieure de $X$}%
  \indexEnglish{least upper bound}{plus petite borne supérieure}%
  \index{supremum de X@supremum de $X$}%
  \indexSymbol{join X@$\join X$}%
  \index{join}%
  \indexSymbol{x join y@$x \join y$}%
  $\forall X \subseteq P, p \in P$ est la \footnotemark{}
  \emphDef{plus petite borne supérieure} \english{least upper bound}
  (ou \emphDef{supremum}) \emphDef{de $X$}
  si $p$ est une borne supérieure de $X$
  et que pour toute borne supérieure $p'$ de $X : p \porder p'$.\\
  Si elle existe nous la notons par \emphDef{$\join X$},
  et nous nommons \emphDef{join} \footnotemark{} cet opérateur $\join$.\\
  Nous abbrévions $\join\{x, y\} =$ \emphDef{$x \join y$}.

  \medskip
  \index{top}%
  \indexSymbol{top@$\top$}%
  Si il existe nous nommons \emphDef{top}
  l'élement $\emphDef{\top} = \join P$.
\end{definition}
\addtocounter{footnote}{-1}%
\footnotetext{
  L'unicité est une conséquence directe des définitions et de la propriété d'antisymétrie.}
\addtocounter{footnote}{1}%
\footnotetext{
  Kolja \textsc{Knauer} dans \cite{chalopin_informatique_2019} chapitre 4
  évoque la possibilité d'utiliser le mot \textit{supremum} à la place de \textit{join}.
  Nous utilisons le mot anglais afin d'éviter les confusions.
  De même ci-dessous pour \textit{top}, \textit{meet} et \textit{bottom}.}

De façon symétrique nous définissons,
\begin{definition}[Borne inférieure]\forcenewline
  \index{borne!inférieure de X@inférieure de $X$}%
  \indexEnglishSub{bound}{lower bound}{borne inférieure}%
  $\forall X \subseteq P, p \in P$ est une
  \emphDef{borne inférieure} \english{lower bound} \emphDef{de $X$}
  si $\forall x \in X, p \porder x$.

  \medskip
  \index{borne!plus grande borne inférieure de X@plus grande borne inférieure de $X$}%
  \indexEnglish{greatest lower bound}{plus grande borne inférieure}%
  \index{infimum}%
  \indexSymbol{meet X@$\meet X$}%
  \index{meet}%
  \indexSymbol{x meet y@$x \meet y$}%
  $\forall X \subseteq P, p \in P$ est la
  \emphDef{plus grande borne inférieure} \english{greatest lower bound}
  (ou \emphDef{infimum}) \emphDef{de $X$}
  si $p$ est une borne inférieure de $X$
  et que pour toute borne inférieure $p'$ de $X : p' \porder p$.\\
  Si elle existe nous la notons par \emphDef{$\meet X$},
  et nous nommons \emphDef{meet} cet opérateur $\meet$.\\
  Nous abbrévions $\meet\{x, y\} =$ \emphDef{$x \meet y$}.

  \medskip
  \index{bottom}%
  \indexSymbol{bottom@$\bottom$}%
  Si il existe nous nommons \emphDef{bottom}
  l'élément $\emphDef{\bottom} = \meet P$.
\end{definition}

\enlargethispage{\baselineskip}%
\begin{definition}[Ensemble partiellement ordonné filtrant]
  \index{ensemble!partiellement ordonné!filtrant}%
  \indexEnglishSub{set}{directed set}{ensemble partiellement ordonné filtrant}%
  $(P, \porder)$ est un \emphDef{ensemble partiellement ordonné filtrant} \english{directed set}
  si $\forall x, y \in P : x \join y$ existe.
\end{definition}

\begin{definition}[Ordre partiel complet]\forcenewline
  \label{CPO}%
  \index{ordre!partiel!complet}%
  \index{CPO}%
  \indexEnglishSub{set}{ordered set!partially ordered set!complete partially ordered set}{ordre partiel complet}%
  $(P, \porder)$ est un \emphDef{ordre partiel complet}
  \english{complete partially ordered set, or CPO}
  si
  \begin{enumerate}
  \item
    l'élément bottom $\bottom$ existe,
  \item
    $\forall X \subseteq P : \join X$ existe.
  \end{enumerate}
\end{definition}



\mySection{Treillis}
Soit un ensemble partiellement ordonné non vide $(T, \porder)$.

\begin{definition}[Treillis]\forcenewline
  \index{treillis}%
  \indexEnglish{lattice}{treillis}%
  $(T, \porder)$ est un \emphDef{treillis} \english{lattice \footnotemark}
  si $\forall x, y \in T : x \join y$ et $x \meet y$ existent.
\end{definition}
\footnotetext{
  Le mot anglais \textit{lattice} renvoie à deux concepts mathématiques différents.
  Il s'agit ici de la notion d'ordre correspondant au mot français \textit{treillis}.
  Le second sens étant une notion géométrique correspondant au mot français \textit{réseau}.}

Un treillis est donc un ensemble non vide muni d'une structure d'ordre
dans laquelle il existe une borne supérieure et une borne inférieure
pour chaque paire d'éléments.

\medskip
\begin{definition}[Treillis complet]\forcenewline
  \index{treillis!complet}%
  \indexEnglishSub{lattice}{complete lattice}{treillis complet}%
  $(T, \porder)$ est un \emphDef{treillis complet} \english{complete lattice}
  si $\forall X \subseteq T : \top$ et $\bottom$ existent.
\end{definition}

Si $T$ est un ensemble fini,
alors tout treillis $(T, \subseteq)$
est un treilli complet.

\medskip
$\forall$ ensemble $X : (\powerset(X), \subseteq)$ est un treillis complet
avec $\forall E \subseteq \powerset(X) : \join E = \cup E$ et $\meet E = \cap E$,
et donc $\top = X$ et $\bottom = \emptyset$.

\bigskip
Remarquons que la structure de treillis est aussi une structure algébrique.

\begin{lemma}[Lemme de connexion]
  $\forall$ treillis $(T, \porder), \forall x, y \in T :$\forcenewline
  $(x \porder y) \iff (x \join y = x) \iff (x \meet y = y)$
\end{lemma}

\begin{theorem}[Propriétés algébriques de join et meet]\forcenewline
  $\forall$ treillis $(T, \porder), \forall x, y, z \in T :$
  \begin{enumerate}
  \item
    \index{loi!associative}
    $(x \join y) \join z = x \join{} (y \join z)$
    \qquad(\emphDef{lois associatives})\\
    $(x \meet y) \meet z = x \meet{} (y \meet z)$
  \item
    \index{loi!commutative}
    $x \join y = y \join x$
    \hspace{6.6em}(\emphDef{lois commutatives})\\
    $x \meet y = y \meet x$
  \item
    \index{loi!d'absorption}
    $x \join{} (x \meet y) = x$
    \hspace{5.8em}(\emphDef{lois d'absorption})\\
    $x \meet{} (x \join y) = x$
  \item
    \index{loi!d'idempotence}
    $x \join x = x$
    \hspace{8.1em}(\emphDef{lois d'idempotence})\\
    $x \meet x = x$
  \end{enumerate}
\end{theorem}

\begin{theorem}[Équivalence du point de vue algébrique]\forcenewline
  Soit un ensemble non vide $E$
  muni de deux relations binaires $\join$ et $\meet$
  qui satistont toutes les propriétés algébriques du précédent théorème.\\
  Soit une relation binaire $\porder$ telle que
  $\forall x, y \in E : (x \porder y) \iff (x \join y) = y$.\\
  Alors $(E, \porder)$ est un treillis,
  tel que $\join$ and $\meet$ correspondent aux opérateurs join et meet.
\end{theorem}



\mySection{Points fixes}
\begin{definition}[Point fixe]\forcenewline
  Soient un ensemble partiellement ordonné $(P, \porder)$,
  et une fonction $f : P \functionArrow P$.

  \medskip
  \index{point fixe}%
  \indexEnglish{fixed point}{point fixe}%
  $x \in P$ est un \emphDef{point fixe} \english{fixed point} de $f$ si $f(x) = x$.
  \footnotemark

  \medskip
  \index{point fixe!plus petit point fixe}%
  \indexEnglishSub{fixed point}{least fixed point}{plus petit point fixe}%
  $x \in P$ est le \footnotemark{}
  \emphDef{plus petit point fixe} \english{least fixed point} de $f$\\
  si $x$ est un point fixe
  et $\forall y \in P : (y$ est un point fixe$) \Impl (x \porder y)$.

  \smallskip
  \indexSymbol{lfp(f)@$\lfp(f)$}%
  Si il existe, nous le notons \emphDef{$\lfp(f)$}.

  \medskip
  \index{point fixe!plus grand point fixe}%
  \indexEnglishSub{fixed point}{greatest fixed point}{plus grand point fixe}%
  $x \in P$ est le \emphDef{plus grand point fixe} \english{greatest fixed point} de $f$\\
  si $x$ est un point fixe
  et $\forall y \in P : (y$ est un point fixe$) \Impl (y \porder x)$.

  \smallskip
  \indexSymbol{gfp(f)@$\gfp(f)$}%
  Si il existe, nous le notons \emphDef{$\gfp(f)$}.
\end{definition}
\footnotetext{
  L'unicité est une conséquence directe des définitions et de la propriété d'antisymétrie.}

\begin{definition}[Application monotone]
  Soient deux ensembles partiellement ordonnés $(P, \porder_P)$ et $(Q, \porder_Q)$.
  \index{application!monotone}%
  \indexEnglishSub{function}{order-preserving function}{application!monotone}%
  L'application \footnotemark{} $f : P \functionArrow Q$
  est dite \emphDef{monotone} (order-preserving) si
  $\forall x, y \in P : (x \porder_P y) \Impl (f(x) \porder_Q f(y))$.
\end{definition}
\footnotetext{
  \index{application}%
  \indexEnglishSub{function}{total function}{application}%
  Une \emphDef{application} \english{total function} est une fonction partout définie.}


\begin{definition}[Application continue]
  Soient deux ordres partiels complets  $(C, \porder_C)$ et $(D, \porder_D)$.
  \IndexScott
  \index{application!continue}%
  \indexEnglishSub{function}{continuous}{application!continue}%
  L'application $f : C \functionArrow D$
  est dite \emphDef{continue} \footnotemark{} \english{continuous}
  si
  $\forall$ ensemble partiellement ordonné filtrant $X \subseteq C :$
  \begin{enumerate}
  \item
    $f(X)$ est un ensemble partiellement ordonné filtrant,
  \item
    $f(\join X) = \join f(X)$.
  \end{enumerate}
\end{definition}
\footnotetext{
  La notion de continuité habituelle en analyse préseve les limites.
  La notion de continuité présentée ici, due à Dana \textsc{Scott},
  préseve le join d'un ensemble partiellement ordonné filtrant.}
Cette \textquote{condition de continuité peut être délicate à vérifier.}
\cite[178]{davey_introduction_2002}

\begin{theorem}[Point fixe de \textsc{Knaster}--\textsc{Tarski}]\forcenewline
  \IndexKnaster
  \IndexTarski
  Soient un treillis complet $(T, \porder)$,
  et $f : T \functionArrow T$ une application monotone.

  Alors $\gfp(f) = \join \{x \in T \mid x \porder f(x)\}$
  et $\lfp(f) = \meet \{x \in T \mid f(x) \porder x\}$.
\end{theorem}

\begin{theorem}[Point fixe de \textsc{Kleene}] \cite[29]{bouissou_analyse_2008}\\
  \IndexKleene
  Soient un treillis complet $(T, \porder)$ avec le top $\top$,
  et $f : T \functionArrow T$ une application continue.
  Alors $\gfp(f) = \meet\limits_{i\in\naturals} \{f^i(\top)\}$.

  \medskip
  Soient un treillis complet $(T, \porder)$ avec le bottom $\bottom$,
  et $f : T \functionArrow T$ une application continue.
  Alors $\lfp(f) = \join\limits_{i\in\naturals} \{f^i(\bottom)\}$.
\end{theorem}



\mySection{Correspondance de \textsc{Galois}}
\label{anx:Galois}%
\begin{definition}[Correspondance de \textsc{Galois}]\forcenewline
  \IndexGalois
  \index{correspondance de Galois@correspondance de \textsc{Galois}}%
  \indexEnglish{Galois@\textsc{Galois}!connection@connection}{correspondance de \textsc{Galois}}%
  Une \emphDef{correspondance de \textsc{Galois}}\footnotemark{}
  \english{\textsc{Galois} connection}
  entre deux ensembles partiellement ordonnés $(A, \porder_A)$ et $(B, \porder_B)$
  est une paire d'applications $\abstraction : A \functionArrow B$ et $\concretization : A \functionArrow B$
  telle que
  $\forall a \in A, b \in B : \abstraction(a) \porder_B b \iff a \porder_A \concretization(b)$.
\end{definition}
\footnotetext{
  Une autre définition parfois utilisée dans certains contextes inverse l'ordre
  (correspondance antitone).
  La définition utilisée ici préserve l'ordre (correspondance monotone).}

\medskip
Elle établit une compatibilité entre les ordres des deux ensembles.

\begin{figureBackground}%
  \input{tikz/treillis/correspondance_de_Galois.tex}%
  \caption{Correspondance de \textsc{Galois}}%
\end{figureBackground}



% Local Variables:
%   ispell-local-dictionary: "french"
% End:
