% -*- coding: utf-8 -*-
\myChapter{Résultats}
\label{chap:Resultats}%
\label{gris}% pour l'exemple de lien interne du chapitre introductif
Ce chapitre présente et analyse les résultats obtenus
dans le but d'évaluer les performances
de nos algorithmes et implémentations.
Il débute par une description des programmes Scheme utilisés
et de la méthodologie employée.


\mySection{Programmes Scheme analysés}
Une
\href{\BITBUCKETSCALAAMLINK/src/master/scala-am/Scheme-examples/}
     {collection de programmes Scheme}
\cite{collectif_scheme_2019}
ont été testés sur un n\oe{}ud du cluster Hydra avec l'implémentation séquentielle de référence.
Les programmes nécessitant plus de 10 minutes d'analyse ont été écartés,
parce que les évaluations non seulement demandent d'être répétées pour être fiables,
mais aussi parce que le nombre d'implémentations à tester
multiplié par le nombre de valeur de $p$ (le nombre d'acteurs/processeurs) testées
donne rapidement des temps totaux de calcul non gérables.
Les programmes Scheme s'analysant trop rapidement ont aussi été écartés,
cette fois pour assurer une certaine précision dans les résultats.
Nous avons toutefois conservé
\href{https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/Scheme-examples/Parallel_AAM_Andersen_2013/factorial_05.scm}
     {\texttt{factorial\_05}}
et
\href{https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/Scheme-examples/Parallel_AAM_Andersen_2013/hailstone_05.scm}
     {\texttt{hailstone\_05}}
qui sont les programmes testés par \cite{andersen_multi-core_2013}
(que nous résumons dans la sous-section \NumNameRef{subsec:EtatArt}).
Rappelons qu'il s'agissait dans cet article
d'une implémentation du $\lambda$-calcul,
et que ces programmes Scheme n'y ont donc pas été testés directement en tant que programme Scheme.

La table ci-dessous résume
les programmes qui ont été choisis parmi ceux qui restaient.
Les hyperliens renvoient vers les sources Scheme.

\textit{Testé sur}
indique sur quels ordinateurs ont été obtenu des résultats :
W pour \textit{SuperWOPR},
B pour \textit{Bertha},
S pour \textit{Serenity}
et H pour \textit{Hydra}.

\textit{\Sharp{} mot} évalue le nombre de mots dans le source du programme
(commentaires exclus, et chaînes de caractères ne comptant que pour un seul mot),
ce qui donne une meilleure idée de leur longueur que de compter le nombre de lignes.

\textit{\Sharp{} !} compte le nombre d'éléments dont le nom se termine par un point d'exclamation.
Convention en Scheme qui indique une fonction modifiant des données.
Les autres étant censées être pures, sans effet de bord.
Donc à l'exception du programme \texttt{qsort}
tous les programmes que nous avons analysés semblent purement fonctionnels.

\textit{\Sharp{} état}
indique le nombre d'états du graphe d'états,
et \textit{\Sharp{} état chaîne initial} le nombre d'états de sa chaîne initiale.
\textit{$\beta$} est leur rapport,
telle que décrit dans la sous-section
\NumNameRef{subsec:BorneSuperieure}.

\textit{\Sharp{} état erreur}
indique le nombre d'états du graphes d'états qui représentent une erreur
dans le programme analysés,
et
\textit{\Sharp{} état final}
indique le nombre d'états finaux.

\textit{$\Tseq$}
indique en secondes
le temps moyen obtenu avec l'implémentation séquentielle de référence
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/src/main/scala/machine/SeqAAMLS.scala}
     {\texttt{SeqAAMLS}}
sur l'ordinateur \hyperref[Bertha]{\textit{Bertha}}.

\begin{table}[H]\centering
  \hspace*{-2em}%
  \input{tex/chapitres/resultats/table_Scheme.tex}
  \hspace*{-2em}%
  \caption{Résumé des programmes Scheme analysés,\\
    avec quelques caractéristiques}
  \label{tab:tableScheme}%
\end{table}

\label{regexScm}%
Nous avons tenté d'analyser sur l'ordinateur \textit{Bertha}
le programme Scheme
\href{https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/Scheme-examples/Sergey/jfp/regex.scm}
     {\texttt{regex}},
plus conséquent.
Cela n'a jamais abouti.
Les tailles des mémoires allouées à la JVM
telle que dans la table \ref{tab:environnements}
ont été doublée six fois de suite,
jusqu'à atteindre la configuration \texttt{-Xms192G -Xmx192G -Xss256M}.
À chaque fois le programme s'est arrêté,
par manque de mémoire.
La dernière fois après 19 heures et 45 minutes.
Le nombre d'états indiqué dans la table
résulte d'un calcul de 10 minutes sur l'ordinateur \textit{Hydra}.
Il ne fait aucun doute que le nombre d'états total est de loin supérieur.



\mySection{Méthodologie}
Chaque évaluation été répétée 13 fois,
les 3 premières mesures du temps d'exécution ont été écartés
et les résultats considérés sont une moyenne sur les 10 dernières.
À l'exception de quelques résultats
stipulant une moyenne sur 30 répétitions (après 5 premières répétitions écartées).
Les premières répétitions sont systématiquement écartées
pour laisser le temps au compilateur à la volée de la JVM
de faire son travail, au moins partiellement.
Après quoi la JVM exécute le code sans avoir à forcément traduire le bytecode en code natif.

Entre chaque répétition,
le programme demande à la JVM d'exécuter son ramasse-miettes \english{garbage collector}
puis prend 1 seconde de pause.
Rien ne garanti que la JVM en profite pour libérer la mémoire,
mais cela augmente les chances que cette opération ne perturbe pas une mesure
suite à une grande consommation mémoire antérieure.

\medskip
La moyenne considérée pour le temps et le surcout
est la moyenne arithmétique.
Tandis que la moyenne géométrique est utilisée
pour l'accélération et l'efficacité,
en vertu des deux premières règles de \cite[1,2]{fleming_how_1986}.

Les moyennes sont calculées sur les métriques individuelles,
comme illustrées ci-dessous pour 3 répétitions.

Moyenne du temps d'exécution séquentiel : $\Tseq = \frac{\Tseq_1 + \Tseq_2 + \Tseq_3}{3}$.

Moyenne d'un temps d'exécution parallèle : $\Tpar = \frac{\Tpar_1 + \Tpar_2 + \Tpar_3}{3}$.

Moyenne pour l'accélération correspondante : $A = \sqrt[3]{A_1 A_2 A_3}$
où chaque $A_i = \frac{\Tseq_i}{\Tpar_i}$.

De même pour l'efficacité.
Et pour le surcout, mais avec la moyenne arithmétique.

\medskip
Les graphiques des temps d'exécution et des accélérations
indiquent une marge d'erreur.
Il s'agit de l'écart type \english{standard deviation},
sauf pour les cas avec 30 répétions
pour lesquelles l'erreur type \english{standard error} a été utilisée.
(Bien que en utilisant l'une ou l'autre nous n'avons pas constaté de différences significatives.)



\mySection{Légende des graphiques}
Les graphiques des résultats adoptent la légende suivante.

\begin{figureBackground}%
  \input{tikz/resultats/legende.tex}%
  \caption{Légende commune pour les graphiques de résultats}%
\end{figureBackground}
\vspace*{-5ex}%
Les couleurs
{\color{PlotLSAPart}rouge},
{\color{PlotCSPart}verte}
et {\color{PlotCPart}bleue}
ont été choisie dans cet ordre connu
pour faciliter les identifications.
Cet ordre correspond à l'ordre de présentation
des 3 familles d'algorithmes du chapitre \NumNameRef{chap:Algorithmes},
du plus simple au plus intéressant.

Pour correspondre aux 3 versions de chacun d'entre-eux,
les courbes en pointillés sont associés aux versions \textit{state}
qui envoient état par état,
les courbes avec tirets aux versions intermédiaires \textit{set}
qui envoient ensemble par ensemble,
et les courbes pleines aux versions \textit{part}
qui découpe en parts (presque) égales.

La croix {\color{PlotSeq}orange}
correspond à l'implémentation séquentielle de référence.

Les courbes en pointillés {\color{PlotGray}grises}
indique le temps idéal ou l'accélération idéale,
dans les graphiques correspondants.

Rappelons que cliquer sur une figure dans un encadré noir
ouvre cette figure dans un fichier PDF distinct,
comme expliqué dans l'introduction
p. \pageref{figureBackgroundHref}.



\mySection{Intégralité des résultats}
Ils sont présentés dans l'ordre des exemples Scheme telle que repris dans la table \ref{tab:tableScheme},
c'est-à-dire par nombre d'états croissants.
Les
\href{\BITBUCKETSCALAAMLINK/src/master/results/}
     {résultats bruts et traités} sont disponibles
dans le dépôt de l'implémentation \cite{pirson_scala-par-am_2019-2}.
Ils sont également visibles
\href{\RESULTSLINK}
     {en ligne}
dans des tables HTML interactives
\cite{pirson_html_2019}.

Les implémentations {\color{PlotCSPart}C-S-state}
et {\color{PlotCSPart}C-S-set}
n'ont pas été calculées sur tous les exemples.
Parce qu'elles sont particulièrement lentes sur certains programmes.
Leur absence dans les résultats de certains programmes
ne signifie pas forcément qu'elles sont mauvaises pour ce programme en particulier,
mais elles ont été écartées dans certaines évaluations
pour s'assurer des temps de calculs totaux raisonnables.

Chaque figure contient en grand l'accélération moyenne,
et en petit le temps moyen,
l'efficacité moyenne, et le surcout moyen.

L'ordinateur \hyperref[SuperWOPR]{\textit{SuperWOPR}}
compte 1 seul processeur de 4 c\oe{}urs.
Avec l'Hyper-Threading cela fait 8 processus possibles simultanément.
Les calculs ont été poussés jusqu'à $p = 8$ acteurs.

L'ordinateur \hyperref[Bertha]{\textit{Bertha}}
compte 2 processeurs de 4 c\oe{}urs.
Avec l'Hyper-Threading cela fait 16 processus possibles simultanément.
Les calculs ont été poussés au-delà, jusqu'à $p = 32$ acteurs.

L'ordinateur \hyperref[Serenity]{\textit{Serenity}}
compte 4 processeurs de 16 c\oe{}urs.
Cela qui fait 64 c\oe{}urs.
Les calculs ont été poussés jusqu'à $p = 64$ acteurs.

Les ordinateurs du cluster \hyperref[Hydra]{\textit{Hydra}}
comptent 2 processeurs de 20 c\oe{}urs.
Cela qui fait 40 c\oe{}urs.
Les calculs ont été poussés jusqu'à $p = 20$ acteurs ou $p = 40$ acteurs.

Les vitesses d'horloge et les caractéristiques
de ces ordinateurs sont différentes.
Nous renvoyons à leur descriptions
dans l'annexe \NumNameRef{anx:environnements}.


\subsectionGraphsThirty{Bertha}{Parallel_AAM_Andersen_2013_factorial_05__35}{factorial\_05}
Le programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/Parallel_AAM_Andersen_2013/factorial_05.scm}
     {\texttt{factorial\_05}}
est tiré de l'article \cite[4]{andersen_multi-core_2013}
que nous résumons dans la sous-section \NumNameRef{subsec:EtatArt}.
C'est l'exemple y obtenant les meilleurs résultats.

Pour nos implémentations parallèles
les résultats sur cet exemple sont exécrables,
et ce pour toutes les implémentations.

Son graphe d'états
pour notre implémentation (qui n'est pas celle du $\lambda$-calcul de l'article)
débute par une chaîne initiale de 5 éléments,
sur les 36 au total, ce qui donne une valeur de $\beta$ de \nombre{0,139}.
Cette partie initiale non parallélisable est importante.

Nous pensons de plus que l'analyse étant tellement courte et rapide,
le surcout de l'activation du système d'acteurs
est non négligeable.
Pour finir,
les variations des mesures lors des 30 répétitions
rend ce test non représentatif.

Notons toutefois que l'ordre des performances de nos 3 familles d'implémentations parallèles
est préservée :
{\color{PlotLSAPart}L-SA},
{\color{PlotCSPart}C-S}
puis {\color{PlotCPart}C}.


\subsectionGraphsThirty{Bertha}{Parallel_AAM_Andersen_2013_hailstone_05__35}{hailstone\_05}
Le programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/Parallel_AAM_Andersen_2013/hailstone_05.scm}
     {\texttt{hailstone\_05}}
est tiré du même article \cite{andersen_multi-core_2013} que le précédent,
comme exemple de programme ayant de moins bons résultats.

Pour nos implémentations parallèles il est moins pire que le précédent.

Nous l'expliquons par une proportion $\beta \simeq \nombre{0,013}$ plus petite,
et un nombre d'états à traiter un peu plus grand.

Cet exemple confirme le constat que ces programmes s'évaluent trop rapidement
(6 millièmes de seconde pour l'implémentation séquentielle)
pour tirer parti de nos implémentations parallèles.



\subsectionSeveralGraphs{Bertha}{OPi_linear_10000}{linear\_10000}
  {\textit{Bertha} et \textit{Serenity}}
Le programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/OPi/linear_10000.scm}
     {\texttt{linear\_10000}}
a été conçu de manière ad hoc
pour produire un graphe d'états complètement linéaire,
donc de proportion $\beta = 1$.

Le gain possible étant nul,
du fait de l'absence de possibilité de parallélisation,
nous en déduisons que
les courbes horizontales dans le graphique de l'accélération
sont révélatrices
d'au moins une partie du surcout intrinsèque de chacun
de nos algorithmes et implémentations.

Elles séparent aussi nettement nos 3 familles d'implémentations parallèles
{\color{PlotLSAPart}L-SA},
{\color{PlotCSPart}C-S}
et {\color{PlotCPart}C}.
Les exemples suivants
confirment cette tendance,
bien qu'il y ait des exceptions.

Notre meilleure implémentation
{\color{PlotCPart}C-part}
résiste bien dans cet exemple.
Son accélération est proche de 1.
Elle exécute à peu de chose près en interne dans un acteur
ce qui se passe dans l'implémentation séquentielle.
Elle semble donc minimiser le surcout dans les parties séquentielles
du graphe d'états.

Il ne faut toutefois pas surinterpréter ces résultats,
parce que une part du surcout résultant de l'interaction entre les acteurs
n'a pas lieu ici, puisque tout se passe de manière séquentielle.

\medskip
\graphs{Serenity}{OPi_linear_10000}{linear\_10000}
Sur l'ordinateur \textit{Serenity}
ce même programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/OPi/linear_10000.scm}
     {\texttt{linear\_10000}}
réduit les écarts entre les 3 familles
{\color{PlotLSAPart}L-SA},
{\color{PlotCSPart}C-S}
et {\color{PlotCPart}C}.
Mais confirme la bonne tenue de l'implémentation {\color{PlotCPart}C-part}.


\subsectionGraphs{Bertha}{OPi_linear_5000_gen4fibonacci_recur}{linear\_5000\_gen4fibonacci\_recur}
Après les deux exemples trop courts pour être représentatifs,
et l'exemple complètement séquentiel,
le programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/OPi/linear_5000_gen4fibonacci_recur.scm}
     {\texttt{linear\_5000\_gen4fibonacci\_recur}}
produit une accélération appréciable avec la meilleure famille d'implémentations {\color{PlotCPart}C}.
Elle croit jusqu'à $p = 8$,
ce qui est le nombre de c\oe{}urs physiques de cette ordinateur.
Puis reste presque constante.
Remarquons que la version \textit{state}
est de performance similaire à la version \textit{set}.

L'efficacité chute néanmoins assez rapidement.

\medskip
Les implémentations {\color{PlotCSPart}C-S-state}
et {\color{PlotCSPart}C-S-set}
n'ont pas été calculées sur cette exemple.
C'est également le cas sur certains programmes suivants.
La raison en est que ces deux implémentations sont particulièrement lentes sur certains programmes.
Pourtant la version {\color{PlotCSPart}C-S-part}
offre une accélération non négligeable.

\medskip
Cet exemple a aussi été écrit de manière ad hoc, pour avoir une chaîne initiale assez longue,
suivie d'un graphe d'états parallélisable
(le calcul par récurrence d'un nombre similaire aux nombres de \textsc{Fibonacci}
mais avec 4 paramètres au lieu de 2).
Sa proportion $\beta = \frac{L}{N} = \frac{15006}{64855} \simeq \nombre{0,231}$
est la plus importante de tous les programmes que nous avons testés.

Les courbes qui alternent pointillés et tirets en {\color{PlotMaroon}marron}
expriment en fonction de $p$
les bornes développées p. \pageref{BorneSuperieureFormule}
dans la sous-section \NumNameRef{subsec:BorneSuperieure}.

La borne supérieure pour l'accélération,
pour une valeur $p = 8$,
donne :
$\frac{pN}{(p - 1)L + N}
= \frac{8 \times 64855}{(8 - 1) \times 15006 + 64855}
\simeq \nombre{3,054}$.

La borne supérieure pour l'efficacité :
$\frac{N}{(p - 1)L + N}
\simeq \nombre{0.382}$.

La borne inférieure pour le surcout :
$(p - 1) \frac{L}{N} \Tseq
\simeq \nombre{15,207}$.

Et la borne inférieure pour le temps de calcul parallèle :\\
$\Tpar
= \frac{\Tseq}{p} \left((p - 1) \frac{L}{N} + 1\right)
\simeq
\nombre{3,074}$.

\smallskip
C'est donc très proche du résultat obtenu par l'implémentation {\color{PlotCSPart}C-S-part}.
La famille d'implémentations {\color{PlotCPart}C} étant même meilleure.\\[1ex]
\begin{tabular}{@{}|l||r||r|r|r|@{}}
  \hline
  Implémentation & Temps & Accélération & Efficacité & Surcout\\
  \hline
  \hline
  {\color{PlotSeq}Séquentielle} & \nombre{9.389} & & & \\
  \hline
  {\color{PlotCSPart}C-S-part} & \nombre{3.080} & \nombre{3.050} & \nombre{0.381} & \nombre{15.253}\\
  \hline
  {\color{PlotCPart}C-state} & \nombre{2.581} & \nombre{3.638} & \nombre{0.455} & \nombre{11.259}\\
  {\color{PlotCPart}C-set} & \nombre{2.544} & \nombre{3.692} & \nombre{0.461} & \nombre{10.959}\\
  {\color{PlotCPart}C-part} & \nombre{2.376} & \nombre{3.952} & \nombre{0.494} & \nombre{9.619}\\
  \hline
\end{tabular}

\medskip
Rappelons que,
puisque ces bornes sont basées sur la supposition que tous les états s'évaluent
en un temps identique,
elles ne sont qu'indicatives.


\subsectionSeveralGraphs{Bertha}{Sergey_kcfa_kcfa_worst_case_20}{kcfa\_worst\_case\_20}
  {\textit{Bertha} et \textit{Serenity}}
Le programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/Sergey/kcfa/kcfa-worst-case-20.scm}
     {\texttt{kcfa\_worst\_case\_20}}
confirme que les meilleurs performances
sont obtenues par les implémentations
{\color{PlotCPart}C}.
Cette fois,
avec une un peu meilleure accélération et meilleure efficacité.

La taille du graphe d'états
est un peu plus grand que celui du programme précédent,
mais du même ordre de grandeur.
Par contre la taille de la chaîne initiale de celui-ci est considérablement plus petite,
et permet une parallélisation presque immédiate de l'évaluation.

L'implémentation {\color{PlotCSPart}C-S-part} est un peu moins bonne pour ce programme.

L'implémentation {\color{PlotCSPart}C-S-set} est meilleure,
alors que sur certains programmes elle est une des pires.

Toutes les implémentations {\color{PlotLSAPart}L-SA}
sont plus lentes que l'implémentation séquentielle.

\newpage
\graphs{Serenity}{Sergey_kcfa_kcfa_worst_case_20}{kcfa\_worst\_case\_20}
Sur l'ordinateur \textit{Serenity}
ce même programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/Sergey/kcfa/kcfa-worst-case-20.scm}
     {\texttt{kcfa\_worst\_case\_20}}
a un comportement similaire jusque $p = 16$ ou 20,
bien que moins bon, puis l'accélération se réduit.


\subsectionSeveralGraphs{SuperWOPR}{Sergey_jfp_primtest}{primtest}
  {\textit{SuperWOPR}, \textit{Bertha}, \textit{Serenity} et \textit{Hydra}}
Sur l'ordinateur \textit{SuperWOPR}
l'analyse du programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/Sergey/jfp/primtest.scm}
     {\texttt{primtest}}
a un comportement étonnant.
L' implémentation {\color{PlotCPart}C-state}
a un comportement sur-linéaire jusque $p = 4$,
qui est le nombre de c\oe{}urs physiques de cette ordinateur.
Plus encore pour {\color{PlotCPart}C-set} jusque $p = 5$.

L'implémentation {\color{PlotCPart}C-part}
est aussi très bonne, avec une efficacité proche de 1 jusque $p = 4$.

L'implémentation {\color{PlotCSPart}C-S-part} ne démérite pas.

\newpage
\enlargethispage*{\baselineskip}%
\null\vspace{-4ex}%
\graphsThirty{Bertha}{Sergey_jfp_primtest__35}{primtest}
\null\vspace{-7ex}%
\graphs{Serenity}{Sergey_jfp_primtest}{primtest}

\newpage
\graphsThirty{Hydra}{Sergey_jfp_primtest__35}{primtest}
Sur ces trois autres ordinateurs
ce même programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/Sergey/jfp/primtest.scm}
     {\texttt{primtest}}
confirme un comportement initial sur-linéaire
avec les implémentations {\color{PlotCPart}C-state} et {\color{PlotCPart}C-set}.

Les implémentations
{\color{PlotCSPart}C-S-state} et {\color{PlotCSPart}C-S-set}
sont les plus mauvaises.


\subsectionGraphs{Bertha}{OPi_tree_tree_16}{tree\_16}
Le programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/OPi/tree/tree_16.scm}
     {\texttt{tree\_16}}
est une tentative ad hoc d'obtenir un graphe d'état
d'une structure d'arbre $m$-aire parfait.
Mais l'implémentation du langage Scheme
en fait un arbre binaire,
alternant des divisions en deux branches avec deux ``étages'' linéaires.

\begin{figureBackgroundHref}%
  \hrefIncludegraphics[width=0.4\textwidth]{graphe_etats/Scheme-examples_OPi_tree_tree_04__TypeSet__bound_100__Classical__step__SeqAAMLS}%
  \caption{Graphe d'états du programme Scheme \texttt{tree\_04}}%
\end{figureBackgroundHref}

L'implémentation {\color{PlotLSAPart}L-SA-part} se révèle la meilleure
avec $p$ suffisamment grand.

\subsectionSeveralGraphs{Bertha}{Sergey_kcfa_kcfa_worst_case_32}{kcfa\_worst\_case\_32}
  {\textit{Bertha} et \textit{Serenity}}
\enlargethispage*{3\baselineskip}\thispagestyle{empty}%
\null\vspace{-8ex}%
\graphs{Serenity}{Sergey_kcfa_kcfa_worst_case_32}{kcfa\_worst\_case\_32}


\subsectionSeveralGraphs{SuperWOPR}{Sergey_kcfa_solovay_strassen}{solovay\_strassen}
  {\textit{SuperWOPR}, \textit{Bertha}, \textit{Serenity} et \textit{Hydra}}
\enlargethispage*{5\baselineskip}\thispagestyle{empty}%
\null\vspace{-8ex}%
\graphs{Bertha}{Sergey_kcfa_solovay_strassen}{solovay\_strassen}

\newpage
\enlargethispage*{\baselineskip}%
\null\vspace{-5ex}%
\graphs{Serenity}{Sergey_kcfa_solovay_strassen}{solovay\_strassen}
\null\vspace{-7ex}%
\graphs{Hydra}{Sergey_kcfa_solovay_strassen}{solovay\_strassen}


\subsectionSeveralGraphs{Bertha}{OPi_gen5fibonacci_recur}{gen5fibonacci\_recur}
  {\textit{Bertha} et \textit{Serenity}}
\enlargethispage*{5\baselineskip}\thispagestyle{empty}%
\null\vspace{-8ex}%
\graphs{Serenity}{OPi_gen5fibonacci_recur}{gen5fibonacci\_recur}


\subsectionSeveralGraphs{Bertha}{Sergey_kcfa_kcfa_worst_case_40}{kcfa\_worst\_case\_40}
  {\textit{Bertha} et \textit{Serenity}}
\enlargethispage*{5\baselineskip}\thispagestyle{empty}%
\null\vspace{-8ex}%
\graphs{Serenity}{Sergey_kcfa_kcfa_worst_case_40}{kcfa\_worst\_case\_40}


\subsectionSeveralGraphs{Bertha}{AlgoDat1_qsort}{qsort}
  {\textit{Bertha}, \textit{Serenity} et \textit{Hydra}}
\enlargethispage*{5\baselineskip}\thispagestyle{empty}%
\null\vspace{-8ex}%
\graphs{Serenity}{AlgoDat1_qsort}{qsort}

\newpage
\graphs{Hydra}{AlgoDat1_qsort}{qsort}
Sur l'ordinateur \textit{Hydra}
le programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/AlgoDat1/qsort.scm}
     {\texttt{qsort}}
donne d'excellents résultats
avec les implémentations
{\color{PlotCSPart}C-S-part}
et {\color{PlotCPart}C-part}.

Ce sont les meilleurs résultats obtenus.
Avec une accélération croissante jusque $p = 20$,
et pour {\color{PlotCPart}C-part} une bonne efficacité.


\subsectionSeveralGraphs{Bertha}{Larceny_Gabriel_takl}{takl}
  {\textit{Bertha} et \textit{Serenity}}
\enlargethispage*{5\baselineskip}\thispagestyle{empty}%
\null\vspace{-8ex}%
\graphs{Serenity}{Larceny_Gabriel_takl}{takl}



\mySection{Résumé du chapitre Résultats}
Les versions des implémentations {\color{PlotLSAPart}L-SA}
sont généralement mauvaises.
Ce qui n'est guère étonnant.
Nous avions souligné l'inconvénient de la barrière
dans le chapitre \NumNameRef{chap:Algorithmes}.

Les implémentations intermédiaires
{\color{PlotCSPart}C-S-set} et {\color{PlotCSPart}C-S-part}
bien que bonnes sur certains exemples,
sont les pires de toutes sur d'autres.

Les versions des algorithmes {\color{PlotCPart}C}
offrent de bonnes accélérations générales,
et {\color{PlotCPart}C-part}
est l'algorithme qui presque systématiquement donne les meilleurs résultats.
Jusqu'à obtenir une accélération de 9 avec $p = 20$
pour l'évaluation du programme
\href{\BITBUCKETSCALAPARAMLINK/src/master/scala-par-am/Scheme-examples/AlgoDat1/qsort.scm}
     {\texttt{qsort}}
sur l'ordinateur \textit{Hydra}.

L'implémentation de notre ultime algorithme confirme
qu'il est en général le meilleur.



% Local Variables:
%   ispell-local-dictionary: "french"
% End:
