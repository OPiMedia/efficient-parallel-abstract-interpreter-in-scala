% -*- coding: utf-8 -*-
\newpage
\mySection{Parallélisation des interpréteurs abstraits}
\label{sec:ParallelisationInterpreteursAbstraits}%
Nous avons évoqué
l'algorithme séquentiel de calcul du point fixe
p. \pageref{subsec:ExemplesGapheDEtats},
sur les deux exemples de graphe d'états.
Rappelons que ce processus boucle
\index{worklist@\texttt{worklist}}%
jusqu'à ce qu'une liste d'états nommée \emphDef{\texttt{worklist}}
devienne vide.
Chacun de ses états est évalué
\index{step@\texttt{step}}%
par une fonction de transition que nous nommons \emphDef{\texttt{step}},
\index{successeur}%
et l'ensemble fini de ses résultats nommés \emphDef{successeurs}
est ajouté à la liste.
Les propriétés des abstractions garantissent que ce processus termine sur un point fixe,
et ce quelque soit l'ordre d'évaluation,
si ce n'est que le processus démarre avec l'état initial dans la \texttt{worklist}.

\begin{figureBackground}%
  \input{tikz/algorithme/worklist_sequentielle.tex}%
  \caption{Principe de l'algorithme séquentiel}%
\end{figureBackground}

L'algorithme séquentiel évalue les états un par un.
Il est exposé plus en détail dans le chapitre \NumNameRef{chap:Algorithmes}.

\medskip
Lorsque l'évaluation d'un état donne plusieurs successeurs,
le graphe d'état se divise en plusieurs branches
et la \texttt{worklist} accumule plusieurs états
qui peuvent être évalués en parallèle.
Les questions étant alors de savoir comment organiser cette évaluation parallèle,
comment partager les états à évaluer et les résultats,
comment éviter les redondances.
Le tout en assurant autant que possible un grain de performance,
et sans commettre les erreurs présentées
dans la section \NumNameRef{sec:parallelisme}.

Nos algorithmes parallèles sont aussi exposées en détail dans le chapitre \ref{chap:Algorithmes}.

\medskip
La sous-section suivante
résume les points important de la littérature abordée qui traite de ces questions.


\mySubsection{État de l'art}
\label{subsec:EtatArt}%
\subsubsection[-- \textit{Multi-core Parallelization of Abstracted Abstract Machines}]
              {\textit{Multi-core Parallelization of Abstracted Abstract Machines}
               \cite{andersen_multi-core_2013}}
\IndexAndersen
\IndexMight
Dans cet article de 2013,
Leif \textsc{Andersen} et Matthew \textsc{Might}
implémentent un interpréteur abstrait parallèle
pour le $\lambda$-calcul,
avec pour objectif [p. 6]
de conserver un équilibre entre la simplicité et la scalabilité.
Ils évaluent leur approche
sur six courts
\href{https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/Scheme-examples/Parallel_AAM_Andersen_2013/}
     {programmes Scheme}
convertis dans ce langage
(en fait cinq calculs de factorielle, et un calcul lié au problème $3n+1$).

Ils partent [p. 1] du constat
que les branches dans le graphe d'états peuvent s'évaluer séparément.
Puis comme cette division en branches est de nature chaotique
et qu'elles peuvent se rejoindre,
ils en concluent que créer un processus léger pour chaque branche serait trop couteux.
En effet,
traiter les branches séparément peut amener à dupliquer des évaluations [p. 3].
Notre graphe d'états de la fonction de \textsc{Fibonacci} p. \pageref{fig:fibonacci}
est un exemple dans lequel de nombreuses branches se séparent et se rejoignent.

Cela les mène [p. 4] à un modèle de producteurs et consommateurs \english{producer et consumer}
avec un nombre fixé [p. 1] de producteurs
qui prennent chacun un état d'une \texttt{worklist},
réalisent les évaluations en parallèles,
et ajoutent les successeurs jamais visités dans la \texttt{worklist}.
Cette \texttt{worklist} est gérée comme une file.

Ils parallélisent aussi [p. 4] l'application des fonctions,
ce qui est un petit bénéfice supplémentaire avec leur machine particulière.

Leur implémentation en Scala
utilise les anciens acteurs de Scala [p. 4],
qui ont depuis été remplacés par l'usage de la bibliothèque Akka
\cite{jovanovic_scala_nodate}.

\medskip
Ils précisent [p. 4] que l'implémentation séquentielle
et l'implémentation parallèle
calculent le même espace d'états,
et en déduisent que la métrique la plus importante est le temps de calcul.

Pour évaluer les performances,
sur 4 c\oe{}urs physiques avec l'Hyper-Threading (c'est-à-dire 8 c\oe{}urs logiques),
ils exécutent les tests quelques fois
afin que le compilateur à la volée de la JVM ait l'occasion de compiler le bytecode,
puis exécutent les tests 20 fois. [p. 4]

\medskip
Les résultats [p. 5] sur l'analyse des cinq calculs de factorielle
atteignent
l'étonnante accélération moyenne de \nombre{8,5} sur un ordinateur
et de \nombre{7,16} sur un autre.
Ils en déduisent que les performances de leur implémentation,
pour l'analyse de ces programmes,
est limitée par le nombre de c\oe{}urs.
Et ne questionne pas cette valeur étonnante
relativement aux seulement 4 c\oe{}urs physiques des ordinateurs utilisés.

\medskip
Les performances sur l'analyse du programme plus large
\href{https://bitbucket.org/OPiMedia/scala-par-am/src/master/scala-par-am/Scheme-examples/Parallel_AAM_Andersen_2013/hailstone_05.scm}
     {\texttt{hailstone\_05.scm}}
[p. 5]
(qui calcule la longueur d'une séquence dans le problème $3n+1$ \cite{lagarias_3x+1_1985})
sont moins bonnes.
L'accélération atteinte est seulement de \nombre{2,3}. [p. 5]
Cela s'explique [p. 6] par une longue phase initiale exclusivement linéaire,
trop importante par rapport au graphe d'états complet.
Ce qui démontre la limitation de ce type de parallélisation,
qui n'est hautement parallélisable que si le graphe d'état convient bien.

\begin{figureBackgroundHref}%
  \hrefIncludegraphics[height=9.75ex]{graphe_etats/Andersen_2013_hailstone}%
  \caption[Graphe d'états de l'analyse de \texttt{hailstone\_05.scm}]
          {Graphe d'états de l'analyse de \texttt{hailstone\_05.scm}
           \cite[6]{andersen_multi-core_2013}}%
  \label{fig:AndersenHailstone}%
\end{figureBackgroundHref}

En conclusion [p. 6],
bien que cet algorithme parallèle est inefficace
sur des exemples ad hoc complètement linéaires,
ils ont obtenu une accélération
sur la plupart des programmes analysés.


\subsubsection[-- \textit{A parallel abstract interpreter for JavaScript}]
              {\textit{A parallel abstract interpreter for JavaScript} \cite{dewey_parallel_2015}}
\IndexDewey
\IndexKashyap
\IndexHardekopf
Dans cet article de 2015,
Kyle \textsc{Dewey},
Vineeth \textsc{Kashyap}
et Ben \textsc{Hardekopf}
reviennent sur le problème
que bien que l'algorithme parallèle d'interprétation abstraite
soit de nature hautement parallélisable,
cette parallélisation est dans les faits grandement limitée
par la nature plus ou moins séquentielle
ou plus ou moins non séquentielle
des graphes d'états des programmes analysés,
et propose une nouvelle approche.

Ils soulignent [p. 1] que la littérature
abordant la parallélisation de l'interprétation abstraite
se consacre en général à des langages statiques de premier ordre.
Ils choisissent d'implémenter leur nouvelle approche pour le langage JavaScript,
pour son caractère dynamique et son usage varié et très répandu. [p. 1]
Ils l'implémentent sur un interpréteur abstrait nommé JSAI
\cite{nystrom_jsai:_nodate}. [p. 7]

Ils présentent [p. 5]
l'algorithme de calcul de point fixe semblable
à ce que nous présentons
dans la section \ref{sec:AlgorithmeSequentiel},
mais utilisant un opérateur d'élargissement \english{widening operator}
réduisant l'espace des états [p. 4].
Puis [p. 5]
une version parallèle exploitant l'idée que nous venons d'évoquer dans cette section,
qui parallélise les évaluations des états présents dans la \texttt{worklist}.

Ensuite [p. 6]
ils présentent une nouvelle approche
qui sépare et exécute en parallèle
l'exploration du graphe d'états
et le processus qui réduit le nombre d'états.

Leurs tests sur des programmes JavaScript réalistes [p. 8]
donnent pour l'approche habituelle
des accélérations atteignant à peine 2,
à l'exception de deux exemples ayant un style résolument hautement fonctionnel
qui atteignent des accélérations supérieures.
[pp. 8, 9]

Avec leur nouvelle approche,
ils obtiennent des accélérations généralement entre 2 et 4
en utilisant 12 processus légers sur 2 processeurs de 6 c\oe{}urs chacun [p. 8].
et de l'ordre de 10 pour les deux mêmes exemples que ci-dessus.
Avec une pointe de \nombre{36.9} pour l'un d'eux ! [p. 2]

Ils précisent [p. 8]
que l'ordre d'évaluation des états
peut largement influencer le temps de calcul,
et que par conséquent il est difficile d'évaluer
dans quelle mesure c'est la parallélisation
qui provoque ces accélérations
et dans quelle mesure c'est une conséquence d'un ordre d'évaluation différent.

Cette nouvelle approche,
bien que séduisante,
n'a pas été retenue pour ce mémoire,
du fait même qu'elle est basée
sur la séparation entre l'exploration du graphe d'états
que nous effectuons
et de l'usage d'un opérateur d'élargissement que nous n'utilisons pas.


\mySubsection{Formulation de bornes pour les différentes métriques}
\label{subsec:BorneSuperieure}
L'article \cite{andersen_multi-core_2013}
résumé ci-dessus
a mis en évidence que certains programmes
ne sont pas adaptés à la parallélisation qui y est développée.
Le cas le plus pathologique étant un graphe d'état entièrement linéaire.

Ci-dessous nous établissons une borne supérieure
pour l'accélération en considérant un graphe d'états
ayant une structure idéale, précédée d'une chaîne (éventuellement de longueur 1).
S'en suit un borne supérieure pour l'efficacité
et des bornes inférieures pour le surcout
et le temps d'exécution parallèle.

\medskip
\index{arbre $m$-aire parfait@m-aire parfait}%
\indexEnglish{perfect $m$-ary tree}{arbre $m$-aire parfait}
Pour commencer,
considérons un
\emphDef{arbre $m$-aire parfait} \english{perfect $m$-ary tree}
\cite{wikipedia_m-ary_2019},
c'est-à-dire un arbre
donc chaque n\oe{}ud interne possède exactement $m$ fils,
et donc toutes les feuilles sont de même niveau.

\begin{figureBackground}%
  \input{tikz/arbre/arbre4.tex}%
  \caption{Arbre $4$-aire parfait de hauteur 3}
  \label{fig:arbre4}%
\end{figureBackground}

Si un graphe d'états a exactement cette structure,
après l'évaluation de l'état initial,
exactement $m$ états sont disponibles pour être évalués.
Ces $m$ états étant les racines de $m$ sous-arbres $m$-aires parfaits.
Par hypothèse ces sous-arbres sont indépendants,
ils peuvent donc être entièrement évalués séparément.
Si nous supposons de plus que le temps pour évaluer chaque états est identique,
une évaluation parallèle du programme ayant un tel graphe d'états
sur $p = m$ processeurs
est la situation idéale.

Remarquons que des structures quelconques
conservant l'équilibre à la places des sous-graphes
ne changerait rien.
En effet, dans cette vision idéale la parallélisation
est de granularité la plus grande possible,
découpant le travail d'un coup en $p$ parties indépendantes.

Et soulignons que la supposition
sur le temps d'évaluation identique pour chaque état
n'est en général pas vérifiée.
Cela implique que ce que nous allons déduire a valeur indicative,
mais n'est pas une limite absolue.

\medskip
Généralisons le graphe d'états que nous considérons
en le faisant précéder d'une
\index{chaîne initiale}%
\emphDef{chaîne initiale} contenant $L \in \naturals*$ états.
Une valeur $L = 1$ correspond
à la situation idéale que nous venons de décrire.

\begin{figureBackground}%
  \input{tikz/arbre/chaine_arbre4.tex}%
  \caption{Arbre $4$-aire parfait de hauteur 3 précédé d'une chaîne initiale}
  \label{fig:chaineArbre4}%
\end{figureBackground}

Soient $N \in \naturals*$ le nombre total d'états
et $N' \in \naturals$ le nombre d'états d'un seul sous-arbre $m$-aire parfait.

L'arbre $m$-aire parfait contient donc $N - L + 1$ états,
et puisque $N - L + 1 = 1 + p N'$,
cela permet d'exprimer
$N' = \frac{N - L}{p}$.

\medskip
Soit $t > 0$
le temps de calcul pour l'évaluation d'un état.

Par conséquent le temps de calcul séquentiel est
$\Tseq = N t$.

L'évaluation parallèle
commence aussi par l'évaluation état par état de la chaîne initiale,
puis évalue en même temps les $p$ sous-arbres,
comme illustré avec $p = 4$ sur la figure \ref{fig:chaineArbre4} ci-dessus.
Donc
$\Tpar = (L + N') t$.

\medskip
\indexSymbol{beta@$\beta$}%
Posons encore,
pour exprimer les formules suivantes d'une double façon,
$\beta \in{} ]0, 1]$
tel que $L = \beta N$.
C'est-à-dire que $\beta$ caractérise la proportion d'états dans la chaîne initiale
relativement aux nombres d'états total.
(C'est une formulation similaire à la proportion $\alpha$ de la loi d'\textsc{Amdahl}
vue p. \pageref{loiAmdahl},
mais qui ne tient compte que de la chaîne initiale.)

\medskip
\label{BorneSuperieureFormule}%
Ce qui donne l'accélération
$\speedup = \frac{\Tseq}{\Tpar}
= \frac{N}{L + N'}
\begin{array}[t]{l@{}}
  = \frac{pN}{(p - 1)L + N}
  = p \left(1 - \frac{(p - 1) L}{(p - 1)L + N}\right)\\

  = \frac{p}{(p - 1) \beta + 1}\\
\end{array}$

\smallskip
L'efficacité
$= \frac{\speedup}{p}
\begin{array}[t]{l@{}}
  = \frac{N}{(p - 1)L + N}
  = 1 - \frac{(p - 1) L}{(p - 1)L + N}\\

  = \frac{1}{(p - 1) \beta + 1}\\
\end{array}$

\smallskip
Le surcout
$= p\,\Tpar - \Tseq
= \left(p (L + N') - N\right) t
= (p - 1) \frac{L}{N} \Tseq
= (p - 1) \beta \Tseq$

\smallskip
Et le temps de calcul parallèle
$\Tpar
= \frac{\Tseq}{p} \left((p - 1) \frac{L}{N} + 1\right)
= \frac{\Tseq}{p} \left((p - 1) \beta + 1\right)$

\bigskip
Les valeurs de ces métriques concernant un graphe d'états idéal,
hormis une chaîne initiale dont la longueur est identifiée,
elles peuvent être considérées comme des bornes atténuant les valeurs
générales de l'accélération linéaire généralement présentée comme l'idéal à atteindre.
Des bornes supérieures pour l'accélération et l'efficacité,
et des bornes inférieures pour le surcout et le temps d'exécution parallèle.

Bien que,
en général,
la valeur de $L$ est petite relativement au nombre total d'états $N$.
Ce qui dans ce cas rend les bornes très proches de leurs valeurs
dans le cas général d'une accélération linéaire.
Ainsi pour $\beta \simeq0$ (et $N$ pas trop petit),
cela borne l'accélération par
$\frac{p}{(p - 1) \beta + 1}
\simeq p$,
ce qui nous ne apprend rien.

Nous pensons toutefois qu'elles peuvent être utiles pour
quantifier la qualité de certaines performances,
en les relativisant vis-à-vis du cas idéal
et en tenant compte d'une chaîne initiale non négligeable.

\medskip
À titre d'illustration,
évaluons ces bornes pour l'exemple de \cite[5]{andersen_multi-core_2013}
dont le graphe d'état est représenté dans la figure \ref{fig:AndersenHailstone}.
Nous y comptons un total de $N = 106$ états,
dont $L = 62$ dans la longue chaîne initiale.
Soit une valeur $\beta = \nombre{0,585}$.

En considérant $p = 8$,
nous obtenons pour l'accélération la borne supérieure
$\speedup
= \frac{N}{(p - 1)L + N}
= \frac{8 \times 106}{(8 - 1) \times 62 + 106}
\simeq \nombre{1,57}$.

Donc finalement le résultat de \nombre{2,3}
que les auteurs ont obtenus \cite[5]{andersen_multi-core_2013}
dépasse les prévisions de notre borne
concernant l'analyse de ce programme.
Notons, que comme déjà exprimé,
leur résultat est étonnant
compte tenu du nombre de c\oe{}urs physiques utilisés.

En ce qui concerne nos bornes,
l'hypothèse que chaque état s'évalue en un temps identique
est probablement trop simpliste.

D'un autre côté nos bornes sont plutôt optimistes,
puisque mis à part la chaîne initiale dont elles tiennent comptent de manière exacte,
elles considèrent le reste du graphe d'états
comme étant parfaitement adaptés à la parallélisation,
ce qui n'est en général pas le cas.
De plus,
elles considèrent également que le nombre de fils $m$
correspond aux nombres de processeurs $p$.
Ce qui ne peut être le cas lorsque nous augmentons le nombre de processeurs.

\medskip
Peut-être est-il possible de formuler
des bornes exactes tenant compte aussi
d'une ou plusieurs chaînes finales.
Éventuellement en considérant le plus long chemin possible sans cycle.

Mais une fois le graphe d'état scindé en plusieurs branches,
il est plus difficile d'en tirer des généralités
que sur le début du processus,
démarrant avec un seul état initial.



\mySubsection{Double application de la loi d'\textsc{Amdahl}}
L'analyse de programmes est un domaine intrinsèquement réflexif.
En effet, comme déjà évoqué,
il s'agit de programmes dont les entrées sont elles-mêmes des programmes.

Or, un programme pouvant bien profiter d'une parallélisation
est un programme qui peut se découper en parties,
plus ou moins indépendantes.

Et de même un programme dont l'analyse peut bien profiter d'une parallélisation
est un programme dont le graphe d'états contient des branches
plus ou moins indépendantes,
c'est-à-dire un programme qui est parallélisable.
Ou plutôt un programme dont l'abstraction est parallélisable.
Puisque le graphe d'états est le résultat
de l'évaluation de l'abstraction du programme analysé,
et non pas du programme lui-même.

De par cette similitude
nous pensons que la loi d'\textsc{Amdahl}
peut s'appliquer deux fois.
Une première fois
qui caractérise le caractère parallélisable
de l'interpréteur abstrait lui-même.
Il s'agit d'un programme hautement parallélisable,
dont le rapport $\alpha$ de la loi d'\textsc{Amdahl} est petit.

Une deuxième fois
sur l'abstraction du programme analysé par l'interpréteur abstrait.
Et là le rapport $\alpha$, en toute généralité, est extrêmement variable.
Il dépend du programme analysé et de l'abstraction,
dans un mélange lui aussi très variable.
C'est un constat bien connu.

Néanmoins nous pensons que cette formulation
en terme de double application de la loi d'\textsc{Amdahl}
illustre le paradoxe du processus de nature hautement parallélisable
limité par ses données,
qui elles, parce qu'elles sont aussi des programmes,
ne sont que partiellement évaluables par parties indépendantes.

\medskip
Peut-être qu'une piste à explorer
serait de considérer des abstractions spécifiques,
scindant le plus possible les graphes d'états en branches indépendantes,
tout en restant suffisamment précises.


\mySubsection{Résumé de cette section sur la parallélisation des interpréteurs abstraits}
La structure du graphe d'états
parcourue lors d'une interprétation abstraite,
lorsqu'elle n'est pas linéaire,
offre la possibilité d'évaluer des branches séparément.
Le problème étant que contrairement à une structure d'arbre
ces branches ne sont pas indépendante,
limitant la possibilité de réellement séparer les branches.

Néanmoins la multiplicité (potentielle) des successeurs d'un état,
et plus généralement,
le nombre d'états accumulés dans une \texttt{worklist},
permet une évaluation des états de façon parallèle,
avec une granularité plus fine.
Le chapitre suivant exploite cette idée.

Dans cette section nous avons également introduit
une borne supérieure de l'accélération possible,
qui dépend de la longueur de la chaîne initiale.
Toutefois cette borne n'est qu'indicative,
parce qu'elle repose sur la supposition
que le temps d'évaluation de tous les états est identique,
ce qui n'est généralement pas le cas.


% Local Variables:
%   ispell-local-dictionary: "french"
% End:
