% -*- coding: utf-8 -*-
\newpage
\mySection{Base du parallélisme}
\label{sec:parallelisme}%
\IndexSutter
{\setlength{\epigraphwidth}{0.74\textwidth}%
  \epigraphSection{\foreignlanguage{english}{In the context of today's CPU landscape, then,
        redesigning your application to run multithreaded on a multicore machine
        is a little like learning to swim by jumping into the deep end---going straight
        to the least forgiving,
        truly parallel environment that is most likely to expose the things you got wrong.}}
                  {--- Herb \textsc{Sutter},
                    \textit{\foreignlanguage{english}{The Free Lunch Is Over: A Fundamental}}\\
                    \textit{\foreignlanguage{english}{Turn Toward Concurrency in Software}},
                    2009
                    \cite{sutter_free_2009}}}

\mySubsection{Considérations générales}
\label{subsec:generales}%
Un
\index{parallélisme}%
\indexEnglishSub{parallel}{computing}{parallélisme}%
\index{programme parallèle}%
\indexEnglishSub{parallel}{program}{programme parallèle}%
\emphDef{programme parallèle}
est un programme conçu explicitement
pour exécuter plusieurs de ses parties simultanément,
dans le but de réduire son temps de calcul.
Pour qu'un programme puisse s'exécuter sur plusieurs processeurs,
il est en effet nécessaire de spécialement le concevoir
en indiquant d'une manière ou d'une autre que telle partie
peut être exécutée en même temps que telle autre.
\footnote{
   Dans les faits nos processeurs modernes exécutent de nombreuses tâches en parallèle
   sans que nous ayons à y penser, d'autres y ayant pensé pour nous.
   L'exécution des instructions machines est décomposée en plusieurs étapes
   \index{chaîne de traitement}%
   \indexEnglish{pipeline}{chaîne de traitement}%
   dans une \emphDef{chaîne de traitement} \english{pipeline},
   superposant plus ou moins les instructions consécutives.
   Certains compilateurs peuvent aussi produire du code utilisant automatiquement
   certaines instructions SIMD (single instruction, multiple data)
   exécutant une même instruction en parallèle sur plusieurs données.}
Alors que par défaut les programmes s'exécutent de façon séquentielle
telle la notion de trace présentée p. \pageref{trace},
un unique élément à la fois dans un ordre entièrement dépendant de l'entrée,
et ce peu importe le nombre de processeurs/c\oe{}urs disponibles.

Dans la suite lorsque nous parlons de processeurs
il faudra aussi entendre c\oe{}urs, de façon plus ou moins indifférenciée.

L'espoir est qu'en utilisant plusieurs processeurs
les programmes s'exécutent d'autant plus vite.
La réalité est que
le développement de programmes parallèles, relativement à leurs versions séquentielles,
amène son lot de nouvelles complications :
ils sont très difficiles à développer
et le gain espéré de performance n'est en rien garanti.
\cite{sutter_free_2009}


\subsubsection[-- Concurrence versus parallélisme]
              {Concurrence versus parallélisme}
Avant tout, débroussaillons le terrain.
Les sens des termes concurrence et parallélisme varient suivant les sources.
Ils sont employés de façon plus ou moins synonyme par certains.
A contrario d'autres les distinguent, de diverses façons.
Dans \cite[15]{erb_concurrent_2012} la concurrence
est présentée comme une propriété d'un programme
et le parallélisme un état d'exécution.
Il y est de surcroît distingué programmation concurrente et concurrence,
et de même programmation parallèle et parallélisme.

Au-delà des variations de sens,
il semble toutefois se dégager l'idée que la
\index{concurrence}%
\emphDef{concurrence}
est un concept plus général d'exécution multiple asynchrone.
Le
\index{parallélisme}%
\emphDef{parallélisme}
concerne lui en particulier
l'exécution d'un seul programme par plusieurs processus légers
(bien que cela soit aussi plus général)
dans le but d'en accélérer l'exécution.

N'ayant pas d'utilité pour le sujet de ce mémoire
toutes ces distinctions sont laissées de côté
et nous nous bornons à ne parler que de parallélisme
malgré le fait que certains éléments qui sont présentés sont plus généraux.


\subsubsection[-- Difficultés de développement]
              {Difficultés de développement}
Réaliser un programme parallèle est un travail difficile.
Alors que nous sommes capables de relativement bien suivre mentalement le fil
de l'exécution d'un programme séquentiel,
l'exécution d'un programme parallèle ressemble rapidement
à un graphe fouillis impossible à suivre.

\medskip
Dans la forme de parallélisme considérée dans ce mémoire,
\index{multithreading}%
le \emphDef{multithreading},
un programme parallèle se compose
de plusieurs
\index{processus léger}%
\indexEnglish{thread}{processus léger}%
\emphDef{processus légers} \english{threads}.
(En fait ces processus légers seront gérés de manière implicite
par la notion d'acteur expliquée dans la sous-section \ref{subsec:acteur}.)
Ces processus légers partagent la même mémoire virtuelle,
mais ont chacun leur propre contexte d'exécution.

Le nombre de processus légers peut varier au cours de l'exécution,
être moins nombreux que le nombre de processeurs disponibles
ou au contraire plus nombreux.
Dans ce cas les processus légers sont exécutés par morceau à tour de rôle.

\medskip
Plusieurs types d'erreurs spécifiques à la programmation parallèle sont possibles.

Lorsque chaque processus léger dispose d'une ressource réclamée par les autres processus légers
le programme est bloqué, chacun attendant les autres.
C'est ce que l'on nomme un
\index{interblocage}%
\indexEnglish{deadlock}{interblocage}%
\emphDef{interblocage} \english{deadlock},
conséquence d'un défaut de conception du programme.

Une erreur similaire a lieu, nommée
\index{interblocage!actif}%
\indexEnglish{livelock}{interblocage actif}%
\emphDef{interblocage actif} \english{livelock},
lorsque les processus légers évoluent tout de même les uns par rapport aux autres
sans toutefois parvenir à faire progresser le programme.

\medskip
Autre erreur,
on parle de
\index{famine}%
\indexEnglish{starvation}{famine}%
\emphDef{famine} \english{starvation}
lorsqu'un processus léger est empêché de s'exécuter,
éventuellement indéfiniment,
par une répartition non équitable des temps de processeur consacré à son exécution.

\medskip
Une opération est dite
\index{atomique}%
\emphDef{atomique}
lorsqu'il est garanti qu'elle sera finalisée
sans être interrompue par un autre processus léger.
Même les opérations qui semblent élémentaires ne le sont généralement pas.

\label{situationDeCompetition}%
Cela mène à une autre erreur.
Lorsque deux processus légers accèdent en parallèle
à une même ressource partagée
et qu'au moins l'un deux la modifie,
si les synchronisations adéquates ne sont pas effectuées
\index{verrou}%
\indexEnglish{lock}{verrou}%
(en posant un \emphDef{verrou} \english{lock} sur cette ressource)
cela produit ce que l'on appelle une
\index{situation de compétition}%
\indexEnglish{race condition ou hazard}{situation de compétition}%
\emphDef{situation de compétition} \english{race condition, ou race hazard},
certains ordres d'exécution produisent des résultats différents
à partir d'une même entrée.
Une partie de code pouvant produire une situation de compétition
est appelée
\index{section critique}%
\indexEnglish{critical section}{section critique}%
\emphDef{section critique} \english{critical section}.

Pour illustrer à quel point le nombre de chemins d'exécution
peut croître très rapidement,
voici une minuscule
\footnote{
  Notons que la minuscule taille de cette classe
  cache une complexité plus grande.
  En effet, le bytecode produit par le compilateur
  est exécuté par la machine virtuelle Java,
  par dessus l'ordinateur réel.}
classe Java,
venant de \cite[180]{martin_clean_2009},
dont la méthode \texttt{getNextId} est une section critique.
On dit d'une telle méthode qu'elle est
\index{thread-safe}%
\emphDef{non thread-safe}.

\medskip
\lstinputlisting[language=Java,
                 classoffset=1,keywordstyle={\color{ListingType}},morekeywords={NewId,int},
                 classoffset=2,keywordstyle={\color{ListingFunction}},morekeywords={getNextId},
                 classoffset=0,
                 backgroundcolor=\color{ListingBackground},numbers=left,
                 caption={Minuscule classe Java non thread-safe},label={lst:NewIdJava}]
                {txt/Java/NewId.java}%

\medskip
Lorsque deux processus légers
exécutent simultanément chacun une fois la méthode \texttt{getNextId}
sur la même instance de la classe \texttt{NewId},
l'exécution peut se chevaucher,
produisant un résultat incorrect
(les deux processus légers recevant la même réponse
alors qu'ils sont censé chacun recevoir une valeur différente).
Pour cet extrêmement simple exemple
le nombre de chemins d'exécution est déjà de l'ordre de \nombre{10000}.
Pire avec un type \texttt{long} au lieu de \texttt{int}
ce nombre est de l'ordre de \nombre{3000000}.
\footnote{
  Nous supposons que ces ordres de grandeur concernent les machines virtuelles Java 32 bits.}
La plupart de ces chemins produisent le bon résultat, mais pas tous.
\cite[180]{martin_clean_2009}

Nous adaptons ce simple exemple en Scala p. \pageref{ExempleAkka}
pour illustrer l'usage des acteurs.

\medskip
La difficulté est d'autant plus sournoise que surgit un
\index{indéterminisme}%
\emphDef{indéterminisme} :
des parties de code pouvant potentiellement s'exécuter en parallèle
peuvent d'une exécution à l'autre réellement s'exécuter simultanément,
avec l'une se terminant avant l'autre ou pas,
ou ne s'exécuter que l'une après l'autre,
avec éventuellement d'autres parties venant s'y mêler.
Le tout dépendant notamment d'éléments extérieurs.
Tester les programmes parallèles sur des environnements différents
peut aider à exhiber les situations de compétition.
\cite{sutter_free_2009}
Néanmoins leur caractère difficilement reproductible
rend leur identification particulièrement difficile.

Ainsi un programme parallèle peut très bien avoir l'air parfaitement correct
parce que les tests produisent les bons résultats,
alors qu'il ne l'est pas.
De subtiles erreurs étant provoquées,
éventuellement très rarement,
par certains agencements dans l'ordre d'exécution.

\medskip
La difficulté d'écrire des programmes parallèles corrects
amène à se poser la question de l'analyse statique de tels programmes.
Toutefois ce mémoire n'abordera que l'analyse de programmes Scheme standards,
et donc séquentiels.
Sur la question spécifique de l'analyse des programmes concurrents/parallèles
vous pouvez notamment consulter
le mémoire de
\IndexStievenart
Quentin \textsc{Stiévenart} \cite{stievenart_static_2014}
ainsi que sa thèse \cite{stievenart_scalable_2018}
pour laquelle a été développé Scala-AM,
l'interpréteur abstrait dont la parallélisation est expliquée dans la suite de ce mémoire.


\subsubsection[-- Gain de performance et limitations]
              {Gain de performance et limitations}
\label{p}%
L'objectif séduisant de la parallélisation
est qu'un programme exécute plusieurs parties simultanément,
réduisant de fait la durée de son exécution.
Soulignons explicitement que pour qu'un programme s'exécute réellement en parallèle
il faut la concordance de deux aspects :
le programme doit avoir été conçu pour cela,
et les ressources physiques nécessaires (en particulier des processeurs/c\oe{}urs)
doivent être réellement disponibles.

\medskip
Pour évaluer le gain de performance
nous représentons la durée d'exécution séquentielle d'un programme par
\indexSymbol{Tseq@$\Tseq$}%
\emphDef{$\Tseq$},
la durée d'exécution parallèle par
\indexSymbol{Tpar@$\Tpar$}%
\emphDef{$\Tpar$},
et le nombre de processeurs/c\oe{}urs disponibles par
\indexSymbol{p@$p$}%
\emphDef{$p$}.

\medskip
Le rapport
$\emphDef{\speedup} = \frac{\Tseq}{\Tpar}$
nommé
\indexSymbol{A@$\speedup$}%
\index{accélération}%
\indexEnglish{speedup}{accélération}%
\emphDef{accélération} \english{speedup}
indique de quel nombre de fois la version parallèle est plus rapide que la version séquentielle.

\index{accélération!linéaire}%
\index{accélération!sous-linéaire}%
\index{accélération!sur-linéaire}%
Idéalement l'accélération vaut $p$ et est alors dite \emphDef{linéaire},
ce qui arrive rarement.
Très exceptionnellement l'accélération est strictement plus grande que $p$
et est dite \emphDef{sur-linéaire},
certaines circonstances tombant particulièrement bien.
Généralement elle est donc strictement plus petite que $p$
et est dite \emphDef{sous-linéaire}.
C'est le cas espéré, le plus éloigné possible du pire cas,
lorsque le programme parallèle est plus lent que sa version séquentielle,
son accélération étant alors strictement plus petite que 1.

Remarquons que la parallélisation d'un programme
sans en changer d'algorithme
ne pouvant qu'au mieux diviser le temps de calcul
par le nombre de processeurs utilisés,
donc par un facteur constant,
cela ne change pas la classe de complexité du programme.

\medskip
Le rapport
$\frac{\text{accélération}}{p} = \frac{\Tseq}{p\,\Tpar}$
nommé
\index{efficacité}%
\indexEnglish{efficiency}{efficacité}%
\emphDef{efficacité} \english{efficiency}
correspond lui au degré d'emploi des $p$ processeurs disponibles.
Cette deuxième métrique
permet de mieux différencier des résultats expérimentaux proches
que ne le permet l'accélération.

Une plus grande quantité de travail augmente l'efficacité.
Alors qu'ajouter un processeur la diminue.
Lorsque l'accroissement de la quantité de travail à effectuer
et l'ajout simultané d'un processeur
conservent le même niveau d'efficacité,
le programme est dit
\index{scalable}%
\emphDef{scalable}.

\medskip
Quand à la différence
$p\,\Tpar - \Tseq$
nommée
\index{surcout}%
\indexEnglish{overhead}{surcout}%
\emphDef{surcout} \english{overhead},
elle quantifie le temps perdu par rapport à la situation idéale.

\begin{table}[H]\centering
  \hspace*{-4em}%
  \begin{tabular}{|@{\ }l@{\ }||@{\ }l@{\ }|@{\ }l@{\,}|@{}|@{\ }l@{\,}|@{}|@{\ }l@{\,}|}
    \hline
    & Plus lent & Sous-linéaire & Linéaire & Sur-linéaire\\
    \hline
    \hline
    Accélération \rule[-2ex]{0pt}{5ex}$= \speedup = \frac{\Tseq}{\Tpar}$
    & $<1$ & $<p$ & $p$ & $>p$\\
    \hline
    Efficacité \rule[-2ex]{0pt}{5ex}$= \frac{A}{p} = \frac{\Tseq}{p\,\Tpar}$
    & $<\frac{1}{p}$ & $<1$ & 1 & $>1$\\
    \hline
    Surcout $= p\,\Tpar - \Tseq = \left(\frac{p}{\speedup} - 1\right) \Tseq$
    & $>(p - 1)\Tseq$ & $>0$ & 0 & $<0$\\
    \hline
    \rule[-2ex]{0pt}{5ex}$\Tpar$
    & $>\Tseq$ & $>\frac{\Tseq}{p}$ & $\frac{\Tseq}{p}$ & $<\frac{\Tseq}{p}$\\
    \hline
  \end{tabular}\hspace*{-4em}
  \caption[Résumé des métriques de performance et des différents cas]
          {Résumé des métriques de performance et des différents cas\\
           (sous-linéaire est la situation espérée, le plus proche possible de linéaire)}
\end{table}

Passons en revue différents surcouts.

\paragraph{Cout dû à l'irréductible partie séquentielle.}
La raison majeure pour laquelle
l'accélération linéaire est rarement atteinte est tout simplement que,
sauf exception,
un programme n'est jamais totalement parallélisable,
contenant des parties intrinsèquement séquentielles,
inhérentes au problème à implémenter.
Dans ces parties certains résultats sont nécessaires
à la production d'autres,
qui eux-mêmes sont nécessaires pour d'autres et ainsi de suite.
Autrement dit,
ce qui sonne comme une évidence,
ce ne sont que les parties relativement indépendantes qui peuvent être exécutées simultanément,
parties qui sont à bien identifier.

\IndexAmdahl
\index{loi!de Amdahl@de \textsc{Amdahl}}%
\label{loiAmdahl}%
La loi d'\textsc{Amdahl}
formalise ce constat que l'accélération possible est limitée
par la partie de programme qui est nécessairement séquentielle.

\begin{law}[Loi d'\textsc{Amdahl}]
  \index{loi!de Amdahl@de \textsc{Amdahl}}%
  \indexSymbol{alpha@$\alpha$}%
  Soit $\alpha \in [0,1]$ la partie d'un programme parallèle
  dont l'exécution est nécessairement séquentielle.
  Donc $(1 - \alpha)$ représente la partie parallélisable.
  L'accélération maximale possible avec $p$ processeurs est donnée par\\
  $\speedup(p)
  = \frac{\Tseq}{\Tpar}
  = \frac{\Tseq}{\alpha \Tseq + (1 - \alpha) \frac{\Tseq}{p}}
  = \frac{p}{1 + (p - 1) \alpha}$.
\end{law}

Selon cette loi
pour une valeur de $\alpha$ fixée non nul l'accélération maximale est bornée par
une asymptote horizontale de hauteur
$\lim\limits_{p\rightarrow\infty} \speedup(p) = \frac{1}{\alpha}$,
et ce quelque soit le nombre de processeurs utilisés.

Cela implique que le calcul parallèle
ne peut vraiment tirer parti que d'un (relativement) petit nombre de processeurs.
Sauf cas particuliers de problèmes qualifiés de
\index{embarrassingly parallel}%
\emphDef{embarrassingly parallel}.

La figure suivante illustre
le problème que le temps de calcul consacré à la partie intrinsèquement séquentielle
ne décroît pas lorsque l'on augmente le nombre de processeurs,
ce qui implique que le gain sur la partie parallélisée
perd en importance relativement au temps total.

\begin{figureBackground}%
  \input{tikz/parallelisme/limite_partie_sequentielle.tex}%
  \caption[Illustration de la limitation due à la partie séquentielle]
          {Illustration de la limitation due à la partie séquentielle\\
           (d'après \cite[59]{mccool_structured_2012})}%
  \label{fig:limite_partie_sequentielle}%
\end{figureBackground}

Dans l'annexe \ref{anx:parallelisme},
nous résumons dans un tableau les valeurs des différentes métriques
associées à la loi d'\textsc{Amdahl}.
Y sont aussi présentés
la loi de \textsc{Gustafson}-\textsc{Barsis}
qui critique certains aspects de celle d'\textsc{Amdahl},
ainsi que de plusieurs graphiques les comparant.


\paragraph{Cout du mécanisme d'exécution parallèle.}
Le travail engendré par la gestion même des processus, bien que dit légers, est un surcout.
Peu importe le mécanisme utilisé,
leurs activations et arrêts ainsi que leur ordonnancement
est un travail qui s'ajoute à celui que serait l'exécution séquentielle du programme.


\paragraph{Cout du travail interne du programme.}
Généralement un programme parallèle
réalise des tâches supplémentaires, toujours relativement à un programme séquentiel.
Les données doivent être séparées,
transmises d'un processus léger à un autre,
et les résultats rassemblés.
Cela engendre de plus des nécessités de synchronisation,
des attentes durant lesquelles des processus légers sont inactifs,
et par conséquent des moments durant lesquels des processeurs peuvent être inutilisés.

On appelle
\index{barrière de synchronisation}%
\indexEnglish{barrier}{barrière de synchronisation}%
\emphDef{barrière de synchronisation} \english{barrier}
les moyens utilisés afin d'assurer que certains processus légers
arrivent à un endroit déterminé de leur exécution
et attendent que tous les autres soient également arrivés.
C'est un point très délicat,
car d'un côté si les synchronisations nécessaires ne sont pas réalisées
cela provoque des situations de compétition.
Et de l'autre côté,
si des synchronisations superflues sont effectuées
cela dégrade les performances,
ou pire, provoque des interblocages.

Le surcout dû à la communication entre processus légers
est d'autant plus grand dans le cadre du calcul distribué,
situation impliquant plusieurs ordinateurs,
la communication entre eux étant beaucoup plus lente que la communication interne.

\bigskip
Tous ces surcouts s'accumulant
expliquent pourquoi le gain de performance espéré n'est jamais garanti,
pourquoi une accélération linéaire est plutôt l'exception que la règle.
Ils pèsent sur les gains potentiels,
pouvant même aller jusqu'à les annuler, ou pire les surpasser.


\subsubsection[-- Nécessité de la parallélisation]
              {Nécessité de la parallélisation}
\label{subsubec:freeLunchIsOver}%
\IndexMoore
\index{loi!de Moore@de \textsc{Moore}}%
Depuis les années 1960 la loi empirique de \textsc{Moore}
stipule que la densité des transistors dans un microprocesseur double
tous les un peu moins de 2 ans.
Cela s'accompagnait jusqu'au début des années 2000
d'un doublement de la rapidité des ordinateurs.
\footnote{
  Par un accroissement de la fréquence d'horloge
  bien que les chaînes de traitement et les caches ont également un impact majeur.}
Chaque nouvelle génération d'ordinateurs permettait plus ou moins automatiquement
pour des programmes de complexité linéaire
de traiter en une durée similaire des données deux fois plus grandes.
Il suffisait d'attendre pour que certaines données
raisonnablement hors de portée deviennent traitables.

Malheureusement depuis c'est terminé,
\textquote{The Free Lunch Is Over} \cite{sutter_free_2009}.
La loi de \textsc{Moore} se poursuit,
les nouveaux ordinateurs sont deux fois plus puissants à chaque échéance,
mais nous bloquons sur les limites physiques
et de fait la rapidité des ordinateurs stagne.
La puissance gagnée est depuis une bonne quinzaine d'années
le résultat d'une multiplication des c\oe{}urs.
Jusqu'à une \textit{éventuelle} nouvelle révolution technologique
il est donc devenu illusoire de ne compter que sur la prochaine génération d'ordinateurs
pour significativement accélérer nos programmes.

\medskip
Pour profiter de la pleine puissance des ordinateurs
nous avons désormais besoin de programmes parallèles,
\footnote{
  Les systèmes d'exploitation multitâches
  tirent automatiquement profit de cette nouvelle puissance
  en permettant l'exécution de plus de programmes en même temps.
  Mais cela ne fait que confirmer le constat,
  ces systèmes d'exploitation étant des programmes parallèles.}
de préférences scalables.
Le parallélisme n'est pas seulement une opportunité,
c'est aussi devenu une nécessité.

\medskip
Pour terminer,
soulignons que paralléliser un programme séquentiel,
donc dans le but de l'accélérer,
n'est probablement pas la première adaptation à tenter
si les algorithmes utilisés ne sont pas suffisamment optimaux.

Nous avons malgré tout parallélisé la machine la plus simple de Scala-AM,
sans chercher à l'optimiser en tant que telle,
dans un but d'expérimentation.
Bien que nous avons grandement accéléré
l'implémentation du Scala-AM originel en mémoïsant les fonctions de hachage.
Voir à ce sujet la section \ref{sec:memoisation} en annexe.

\IndexHugo
\IndexGuitry
\epigraphSection{\null\hfill
                 Victor \textsc{Hugo} s'est contredit pour être sûr d'avoir tout dit.}
                {--- Sacha \textsc{Guitry},
                  \textit{Toutes réflexions faites}, 1947 \cite{guitry_toutes_1947}}


\mySubsection{Modèle d'acteur et bibliothèque Akka}
\label{subsec:acteur}%
\IndexShakespeare
{\setlength{\epigraphwidth}{0.8\textwidth}%
  \epigraphSection{Le monde entier est un théâtre,
--- et tous, hommes et femmes, n'en sont que les acteurs.
--- Tous ont leurs entrées et leurs sorties,
--- et chacun y joue successivement les différents rôles
--- d'un drame en sept âges.}
                  {--- William \textsc{Shakespeare},
                    \textit{Comme il vous plaira},\\ 1599
                    \cite[Acte \RN{2} Scène \RN{7} Ligne 138]{shakespeare_comme_1872}}}

Comme expliqué dans la précédente sous-section \NumNameRef{subsec:generales}
l'écriture de programmes parallèles est difficile.
Elle l'est d'autant plus lorsque les primitives utilisées sont de bas niveau,
laissant aux programmeurs le soin de
gérer les processus légers et de protéger les données.
De telles primitives permettent certes un contrôle total si elles sont utilisées à la perfection,
mais sont facilement source d'erreurs délicates.

Pour faciliter la programmation
plusieurs modèles de parallélisme haut niveau ont été élaborés.
Le choix pour l'implémentation de ce mémoire s'est porté sur le modèle d'acteur
au travers de la bibliothèque Akka \cite{akka_akka_nodate}.
C'est un choix naturel en Scala \cite{scala_scala_nodate},
le langage de programmation
dans lequel est écrit l'interpréteur abstrait à paralléliser Scala-AM.


\subsubsection[-- Modèle d'acteur]
              {Modèle d'acteur}
Le
\index{modèle d'acteur}%
\indexEnglish{actor model}{modèle d'acteur}%
\emphDef{modèle d'acteur} \english{actor model}
est un modèle de programmation parallèle
\footnote{
  En fait de programmation concurrente,
  mais nous restons concentrés sur le parallélisme.}
créé
\IndexHewitt
\IndexBishop
\IndexSteiger
Carl \textsc{Hewitt},
Peter \textsc{Bishop}
et Richard \textsc{Steiger}
en 1973 \cite{hewitt_universal_1973},
à la fois comme outil théorique destiné à la compréhension
et comme base pratique d'implémentation.
Il a été popularisé par son usage dans le langage de programmation Erlang.

Il est constitué par un ensemble d'entités isolées les unes des autres nommées acteurs.
Ces acteurs communiquent entre eux uniquement par envoi de messages asynchrones,
et s'exécutent entre eux de façon \textit{parallèle}.
L'exécution d'un acteur étant déclenché lorsqu'il réceptionne un message.
Il s'agit donc d'un modèle de
\index{programmation événementielle}%
\indexEnglish{event-driven programming}{programmation événementielle}%
\emphDef{programmation événementielle}
\english{event-driven programming}.

\begin{figureBackground}%
  \input{tikz/parallelisme/acteurs.tex}%
  \caption{Trois acteurs envoyant des messages}%
  \label{fig:exempleActeurs}%
\end{figureBackground}

Un
\index{acteur}%
\emphDef{acteur}
est une entité \textit{séquentielle}, s'exécutant dans un seul processus léger,
composée de :
\begin{itemize}[nosep]
\item
  Un contexte d'exécution qui lui est propre.
\item
  Un état local contenant des variables modifiables par lui seul.
\item
  Une ou plusieurs
  \index{adresse}%
  \emphDef{adresses}
  permettant aux autres acteurs les connaissant de lui envoyer des messages.
  (Cette adresse ne correspond pas au concept d'adresse mémoire.)

  Un
  \index{message}%
  \emphDef{message}
  est simple, non mutable,
  et peut être transmis au travers d'un réseau de façon transparente.

  Un message peut contenir l'adresse d'un acteur.
  Ce qui permet notamment à un acteur de connaître l'adresse
  de l'émetteur d'un message reçu et ainsi de pouvoir répondre.
\item
  Une \emphDef{boîte aux lettres} \english{mailbox}
  recevant les message, un à la fois,
  et les stockant dans l'ordre d'arrivée.
  Elle a une structure de file \english{queue, FIFO first-in, first-out}.
\end{itemize}

\medskip
Un acteur est capable de :
\begin{itemize}[nosep]
\item
   Exécuter du code séquentiellement
   et changer son état interne (d'un état cohérent à un autre état cohérent),
   à la suite de la réception d'un message.
\item
   Réceptionner un message à la fois, de sa boîte aux lettres, dans l'ordre de leur arrivée.
\item
   Envoyer des messages aux acteurs dont il connaît une adresse.
\item
   Créer de nouveaux acteurs. Il en connaît alors l'adresse.
\item
   Se supprimer.
\end{itemize}

\medskip
Un acteur après avoir été créé
est en attente jusqu'à la réception d'un premier message.
Et chaque réception de message
lance l'exécution séquentielle du code associé,
puis se place en attente d'une prochaine réception.

La pierre angulaire de ce système
est que tous les acteurs exécutent leur code de façon asynchrone.
De même pour les messages envoyés, qui sont également acheminés de façon asynchrone,
indépendemment des acteurs.
C'est-à-dire que pour autant que des ressources matérielles soient disponibles,
tous les acteurs agissent en parallèle.

Par conséquent les messages envoyés se transmettent dans un ordre indéterminé.

\medskip
\label{acteurPasEquivalentThread}%
L'implémentation du modèle fait au mieux
pour organiser ce parallélisme de façon performante,
dispatchant automatiquement
les codes à exécuter des acteurs traitant un message
dans des processus légers,
qui sont eux-mêmes dispatchés sur les processeurs disponibles.


\subsubsection[-- Avantage du modèle d'acteur]
              {Avantage du modèle d'acteur}
C'est un modèle de haut niveau,
qui gère de façon implicite les processus légers
et nous soulage de l'utilisation de primitives de synchronisation
et autres considérations techniques,
laissant plus facilement la place à la logique à implémenter.
Ce qui permet aussi, indirectement,
de gérer plus facilement
d'autres éléments qui sont eux partagés et nécessitent des synchronisations.

\medskip
De par le principe de la boîte aux lettres
le programme n'a pas à surveiller de façon explicite la venue de messages.
Ils s'accumulent d'eux-mêmes jusqu'à être réceptionnés
et les acteurs s'activent d'eux-mêmes sur le code associé
pour chaque message réceptionné.

\medskip
De par le caractère privé des états internes à chaque acteur,
il est impossible de subir des situations de compétition sur ces états.
C'est très appréciable puisque,
comme vu dans la sous-section \NumNameRef{subsec:generales} p. \pageref{situationDeCompetition},
ces erreurs sont particulièrement difficiles à éviter.
Notons tout de même que cela ne vaut pas pour
les données extérieures aux acteurs
et qui leurs sont accessibles.

Soulignons aussi que ce modèle
ne protège ni des interblocages ni des famines.

\medskip
Les acteurs peuvent être exécutés de façon transparente par des ordinateurs distants,
les messages transitant par un réseau.

\medskip
Les acteurs sont des entités plus légères que les processus légers.
Suivant les implémentations
il est possible d'en créer des milliers voir même des millions
tout en ne mobilisant que peu de processus légers.
\cite[26]{roestenburg_akka_2017}


\subsubsection[-- Comparaison avec la programmation orientée objet]
              {Comparaison avec la programmation orientée objet}
Par certains aspects les acteurs ressemblent beaucoup aux objets.
Un acteur est un peu un objet privé, sans notion d'héritage,
dont les envois de messages synchrones par appel de méthodes
sont remplacés par d'explicites envois de messages asynchrones.

Différence majeure les acteurs possèdent une boîte aux lettres,
et surtout s'exécutent entre eux en parallèles.

La ressemblance est accentuée
du fait de certaines implémentations dans des langages objets.
Dans l'implémentation du modèle d'acteur dans Scala par la bibliothèque Akka
chaque acteur est une classe étendue par le trait
\href{\AKKADOCLINK/akka/actor/Actor.html}
     {\texttt{{\small akka.actor.}Actor}}
\footnote{
    Cet hyperlien et les suivants de cette sous-section pointent
    vers les documentations officielles de Akka ou de Scala.}.


\subsubsection[-- Simple exemple d'utilisation des acteurs en Scala avec Akka]
              {Simple exemple d'utilisation des acteurs en Scala avec Akka}
\label{ExempleAkka}%
En guise d'entraînement à la logique de réflexion
nécessaire à la bonne compréhension des algorithmes parallèles du chapitre \ref{chap:Algorithmes},
prenons le temps d'illustrer sur un petit programme d'exemple l'usage des acteurs,
en Scala avec la bibliothèque Akka.
Le programme suivant consiste en la réimplémentation en tant qu'acteur
de la classe Java non thread-safe \texttt{NewId} du listing \ref{lst:NewIdJava},
ainsi que de deux acteurs qui la sollicitent en parallèle.

Le programme complet
\href{\BITBUCKETLINK/src/master/Akka_simple_example/}
     {\texttt{newId}}
(\href{\BITBUCKETLINK/src/master/Akka_simple_example/newId/src/main/scala/be/opimedia/newId/NewIdApp.scala}
      {\texttt{NewIdApp.scala}})
est disponible en ligne
dans le dépôt \cite{pirson_efficient_2018} contenant ce document.
Voyons-en les éléments importants morceau par morceau,
dans l'ordre du code source,
en esquivant notamment la partie des importations de paquetages.

\input{tex/chapitres/contexte/exempleAkka.tex}


\mySubsection{Résumé de cette section sur le parallélisme}
Nous avons exposé à la fois ce que permet le parallélisme dans l'idéal,
accélérer nos programmes,
et ce qui le limite en pratique.
Au passage nous avons néanmoins souligné l'utilité, voir même la nécessité,
de considérer la parallélisation comme moyen d'améliorer les performances.

Les diverses métriques de performances sont utilisées dans le chapitre
\NumNameRef{chap:Resultats}.

\medskip
Nous nous sommes aussi penché sur les difficultés de développer de tels programmes,
et avons présenté le modèle d'acteur qui de part son mode de fonctionnement
peut aider à les réduire.


\medskip
Avec toutes ces notions
nous pouvons maintenant aborder
le sujet proprement dit de la parallélisation de l'interprétation abstraite.



% Local Variables:
%   ispell-local-dictionary: "french"
% End:
