% -*- coding: utf-8 -*-
\myChapter{Introduction}
\setcounter{page}{0}%
\thispagestyle{zero}%
\footnotetext{\IndexKellyBootle
  \textquote{\foreignlanguage{english}{Should array indices start at 0 or 1? My compromise of 0.5 was rejected without, I thought, proper consideration.}}
  (Stan \textsc{Kelly-Bootle})}%
\addtocounter{footnote}{-1}%
\mySection{Contexte et objectif du mémoire}
L'interprétation abstraite est une technique d'analyse statique,
qui permet de prouver des propriétés sur des programmes
(ou de mettre en évidence la présence de bogues)
en en évaluant une sur-approximation correcte,
appelée abstraction.
En particulier la technique d'abstraction de machine abstraite
\english{abstracting abstract machine, AAM}
\cite{van_horn_abstracting_2010},
à partir d'un interpréteur concret opérant état par état,
dérive systématiquement un interpréteur abstrait.
Son principe de base consiste à démarrer d'un état initial
(une abstraction du programme à analyser),
puis à appliquer itérativement sur chaque état
une fonction de transition abstraite qui renvoie un ensemble d'états,
jusqu'à l'obtention d'un point fixe.
Ce processus parcourt/construit un graphe d'états.

Cette technique qui contourne une impossibilité théorique
reste toutefois lente en pratique
lorsqu'elle analyse des programmes écrits dans des langages dynamiques
disposant de fonctions d'ordre supérieure.

L'objectif du mémoire est d'expérimenter la parallélisation
de l'interprétation abstraite
pour en améliorer la rapidité.

\medskip
Ce mémoire introduit à la théorie de l'interprétation abstraite,
puis à la notion de parallélisme,
en particulier du modèle d'acteur,
pour en arriver aux possibilités de parallélisation de l'interprétation abstraite.
Les limites intrinsèques à ce problème sont abordées,
notamment au travers de la formulation d'une borne supérieure de l'accélération,
fournie par le cas improbable idéal.

Sont ensuite présentés plusieurs algorithmes parallèles,
en partant d'idées simples et inefficaces,
pour les adapter progressivement et atteindre une accélération acceptable.
Les algorithmes ont été implémentés
dans (une restriction de) l'interpréteur abstrait Scala-AM \cite{stievenart_scala-am:_2017}.
Les performances de chaque implémentation
sont évaluées sur l'analyse de programmes Scheme,
en les comparant aux performances d'une implémentation séquentielle.



\mySection{Contributions}
Nous développons
p. \pageref{subsec:BorneSuperieure}
une borne supérieure indicative de l'accélération possible
pour une implémentation parallèle d'un interpréteur abstrait,
analogue partiel de la loi d'\textsc{Amdahl}
appliquée sur les programmes analysés.

\medskip
Nous introduisons p. \pageref{sec:StructuresWorklist}
une structure hybride pour la \texttt{worklist},
menant aux algorithmes parallèles que nous nommons \textit{set}.

\medskip
La section \ref{sec:AlgorithmesParalleles}
présente plusieurs variantes de nouveaux algorithmes parallèles
pour l'interprétation abstraite.

\medskip
Nous avons
\href{\BITBUCKETSCALAPARAMLINK}
     {implémenté} \cite{pirson_scala-par-am_2019-2}
tous les algorithmes présentés dans ce chapitre
dans l'interpréteur abstrait Scala-AM.
Il s'agit d'une première parallélisation de cet interpréteur abstrait.

\medskip
Chacun de ses algorithmes implémentés
a été évalués sur l'analyse de programmes Scheme.
L'intégralité des
\href{\RESULTSLINK}
     {données des résultats} \cite{pirson_html_2019}
sont disponibles en ligne
et analysées dans le chapitre
\NumNameRef{chap:Resultats}.



\mySection{Structure de ce document}
Le chapitre \ref{chap:Contexte}
expose le contexte théorique.
Il débute par la présentation de l'interprétation abstraite,
puis aborde le parallélisme.
Ensuite il relie ces deux domaines
pour introduire le sujet proprement dit de
la parallélisation de l'interprétation abstraite,
en exposer les principes et limites.

Le chapitre \ref{chap:Algorithmes}
expose l'algorithme séquentiel de base de calcul de point fixe,
puis plusieurs algorithmes parallèles développés sur cette base.
Du plus simple au plus intéressant.

Le chapitre \ref{chap:Implementation},
après quelques explications sur l'interpréteur abstrait Scala-AM,
précise les détails des implémentations des algorithmes du chapitre précédent.

Le chapitre \ref{chap:Resultats}
présente et analyse les résultats des évaluations de leur performance.

Finalement,
le chapitre \ref{chap:Conclusion}
résume le tout.

\medskip
Sont relégués en \hyperref[part:Appendix]{annexe} un ensemble de précisions diverses
qui bien que pouvant éclairer certains points,
ne sont pas absolument nécessaires aux déroulement du mémoire.

\medskip
Le document se termine par une partie \hyperref[chap:Listes]{Références}
% workaround
rassemblant après plusieurs listes,
l'\hyperref[sec:Index]{Index}
et la \hyperref[chap:Bibliographie]{Bibliographie}.



\mySection{Conventions utilisées dans ce document}
Tout du long de ce document
les termes définis sont indiqués en \emphDef{italique orange}
et sont repris dans l'\hyperref[sec:Index]{Index}.

Dans le but de faciliter l'accès à la littérature sur les sujets abordés
qui est majoritairement anglophone
--- et finalement pour faciliter la compréhension même des francophones ---
la plupart des termes définis précisent
leur correspondant anglais en \english{gris}.
Ils sont repris à part dans l'index.

Quelques mots restent en anglais, faute d'avoir trouvé de traduction satisfaisante.

\medskip
Les hyperliens internes à ce document électronique sont colorés de
\hyperref[gris]{gris},
tandis que ceux externes sont colorés de
\href{\RESULTSLINK}{bleu}
(à l'exception des figures comme ci-dessous).

\begin{figureBackgroundHref}%
  \label{figureBackgroundHref}%
  \hspace*{-0.5cm}\hrefIncludegraphics[height=12ex]{resultats/Bertha/yerrorbars/yerrorbars__resume__benchmarks__TypeSet__Sergey_jfp_primtest__35__speedup}\hfil
  \begin{minipage}[b]{\textwidth-4cm}
    Cliquer sur une figure dans un encadré noir comme celui-ci
    ouvre un fichier PDF permettant de mieux la visualiser.
    \textit{À la condition} que ce fichier PDF soit présent au bon endroit,
    relativement à ce document.
  \end{minipage}%
\end{figureBackgroundHref}



% Local Variables:
%   ispell-local-dictionary: "french"
% End:
