DOCUMENTS = ../Efficient-Parallel-Abstract-Interpreter-in-Scala--Second-Presentation--Olivier-Pirson-20180320--slides.pdf

.SUFFIXES:

DVIPS      = dvips
DVIPSFLAGS =

PSPDF      = ps2pdf
PSPDFFLAGS = -sPAPERSIZE=a4

TEX      = latex
TEXFLAGS =


DOT   = dot
RM    = rm -f
SHELL = sh



###
# #
###
.PHONY:	all

all:	$(DOCUMENTS)

eps/state_graph_abs.eps:	eps/_src/test__OPi__abs___Scheme__AAM__TypeSet__Classical.dot
	$(DOT) -Teps -o$@ $<

eps/state_graph_fibonacci.eps:	eps/_src/test__OPi__fibonacci_recur___Scheme__AAM__TypeSet__Classical.dot
	$(DOT) -Teps -o$@ $<

eps/state_graph_fibonacci_iter.eps:	eps/_src/test__OPi__fibonacci_iter___Scheme__AAM__TypeSet__Classical.dot
	$(DOT) -Teps -o$@ $<



#########
# Rules #
#########
.PRECIOUS: %.aux %.bcf %.dvi %.idx %.ind %--printable-twoside.ps %.ps

%.dvi:	%.tex
	$(TEX) $(TEXFLAGS) $<
ifeq ($(QUICK),)
	$(TEX) $(TEXFLAGS) $<
endif

../%.pdf:	%.ps
	$(PSPDF) $(PSPDFFLAGS) $< $@

%.ps:	%.dvi
	$(DVIPS) $(DVIPSFLAGS) -o $@ $<



################
# Dependencies #
################
Efficient-Parallel-Abstract-Interpreter-in-Scala--Second-Presentation--Olivier-Pirson-20180320--slides.dvi:	eps/state_graph_abs.eps eps/state_graph_fibonacci.eps eps/state_graph_fibonacci_iter.eps \
	sty/opislides.sty \
	tex/context.tex tex/problematic.tex tex/next.tex tex/parallelism.tex tex/references.tex



#########
# Clean #
#########
.PHONY:	clean cleanDvi cleanPdf cleanPs distclean overclean

clean:
	$(RM) *.aux *.bbl *.blg *.brf *.idx *.ilg *.log .log *.nav *.out *.snm *.toc *.tdo

cleanDvi:
	$(RM) *.dvi

cleanPdf:
	$(RM) $(DOCUMENTS)

cleanPs:
	$(RM) *.ps

distclean:	clean cleanDvi cleanPs

overclean:	distclean cleanPdf
