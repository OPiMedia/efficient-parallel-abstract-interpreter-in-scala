addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "0.14.9")  // to build JAR of the complete project
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")  // Scala style checker
