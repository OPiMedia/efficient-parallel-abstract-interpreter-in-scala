.. -*- restructuredtext -*-

=====
newId
=====

This is the complete source (in Scala with Akka) of
the simple example for the illustration of the actor model
in the final document of the master thesis.

The compiled JAR file is available here: `newId.jar`_.

.. _`newId.jar`: https://bitbucket.org/OPiMedia/efficient-parallel-abstract-interpreter-in-scala/downloads/newId.jar
