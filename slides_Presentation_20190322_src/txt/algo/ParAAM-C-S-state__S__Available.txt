

foreach (state, actor)
from current worklist
and available Eval actors
    send state to actor

if worklist is empty
   and all Eval actors available
    send finished
