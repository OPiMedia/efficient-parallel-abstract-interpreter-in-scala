worklist =
  worklist$\cup${initial state}
repeat
    send current worklist
    wait
    receive new worklist
until worklist is empty
